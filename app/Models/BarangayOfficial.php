<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangayOfficial extends Model
{
	protected $fillable = ['barangay_official_name', 'position', 'date_elected', 'end_term', 'display_image', 'description','barangay_id'];
	protected $table = 'barangay_officials';

	protected $primaryKey = 'barangay_official_id';
	

	public $timestamps = false;

	
}
