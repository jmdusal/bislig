<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
	protected $fillable = ['title','display_image', 'description', 'date_created', 'barangay_id'];
	protected $table = 'announcement';

	protected $primaryKey = 'announcement_id';

	public $timestamps = false;


	// public function barangay()
	// {   
	// 	return $this->belongsTo(Barangay::class, 'barangay_id');
	// }
}
