<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Barangay extends Model
{
	protected $fillable = ['barangay_name', 'display_image', 'description', 'user_id'];
	protected $table = 'barangay';

	protected $primaryKey = 'barangay_id';
	//protected $uniq = 'barangay_name';

	public $timestamps = false;


	// public function announcements()
	// {
	// 	return $this->hasMany(Announcement::class);
	// }
}
