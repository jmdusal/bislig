<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangayWelcome extends Model
{
	protected $fillable = ['title', 'display_image', 'description', 'about_description', 'barangay_id'];
	protected $table = 'barangay_welcome';

	protected $primaryKey = 'barangay_welcome_id';

	public $timestamps = false;
}
