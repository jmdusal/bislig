<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangayVideo extends Model
{
	protected $fillable = ['embed_video', 'barangay_id'];
	protected $table = 'barangay_video';

	protected $primaryKey = 'barangay_video_id';

	public $timestamps = false;
}
