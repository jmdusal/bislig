<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangayGallery extends Model
{
	protected $fillable = ['display_image', 'date_taken','barangay_id'];
	protected $table = 'barangay_galleries';

	protected $primaryKey = 'barangay_gallery_id';

	public $timestamps = false;
}
