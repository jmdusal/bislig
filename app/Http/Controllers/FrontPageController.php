<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CityOfficial;
use App\Models\Barangay;
use App\Models\BarangayOfficial;
use App\Models\BarangayGallery;
use App\Models\Announcement;
use App\Models\BarangayWelcome;
use App\Models\BarangayVideo;


use Carbon\Carbon;
use Auth;
use DB;

class FrontPageController extends Controller
{


	// HOME PAGE

	public function homepage($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;
		$barangay = DB::table('barangay')->where('barangay_name', '=', $barangay_name)->get();
		$barangays = DB::table('barangay')->get();

		$barangay_welcome = DB::table('barangay_welcome')
		->join('barangay', 'barangay_welcome.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_welcome.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();

		$barangaygallery = DB::table('barangay_galleries')
		->join('barangay', 'barangay_galleries.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_galleries.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();

		$announcement = DB::table('announcement')
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->select('announcement.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();

		$latest_announcement = Announcement::where('date_created', Announcement::max('date_created'))
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->select('announcement.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->orderBy('date_created', 'desc')->limit(1)->get();
		
		if($barangay_name == true){
			return view('welcome-body', compact('barangays', 'barangay','barangay_welcome', 'barangaygallery', 'announcement', 'latest_announcement'));
		}else{
			return false;
		}
	}






	// BARANGAY OFFICIAL PAGE

	public function barangayofficials($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;

		$barangayofficials = DB::table('barangay_officials')
		->join('barangay', 'barangay_officials.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_officials.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();

		$barangay_officials = DB::table('barangay_officials')
		->join('barangay', 'barangay_officials.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_officials.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->where(function($arrgument){
			return $arrgument->where('position', "Barangay Captain")
			->OrWhere('position', "SK Member")
			->OrWhere('position', "SK Chairman")
			->OrWhere('position', "Barangay Councilor")
			->OrWhere('position', "Barangay Secretary")
			->Orwhere('date_elected', '<=', 'end_term');
		})
		->get();

		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();
		
		if($barangay_name == true){
			return view('front-page.barangay_officials', compact('barangay' ,'barangayofficials','barangay_officials'));
		}else{
			return false;
		}


	}




	// BARANGAY GALLERY PAGE

	public function gallery($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;

		$barangaygallery = DB::table('barangay_galleries')
		->join('barangay', 'barangay_galleries.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_galleries.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();

		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();

		if($barangay_name == true){
			
			return view('front-page.barangay_gallery', compact('barangay', 'barangaygallery'));
		}else{
			return false;

		}

	}




	// BARANGAY ABOUT PAGE

	public function about($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;

		$barangay_welcome = DB::table('barangay_welcome')
		->join('barangay', 'barangay_welcome.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_welcome.*', 'barangay.barangay_name', 'barangay.description')
		->where('barangay.barangay_name', '=', $barangay_name)
		->get();
		


		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();

		if($barangay_name == true){
			return view('front-page.about-barangay', compact('barangay', 'barangay_welcome'));
		}
	}






	// BARANGAY VIDEO PAGE

	public function video($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;

		$barangayvideo = DB::table('barangay_video')
		->join('barangay', 'barangay_video.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_video.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->paginate(10);

		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();

		if($barangay_name == true){
			
			return view('front-page.barangay_video', compact('barangay', 'barangayvideo'));
		}else{
			return false;

		}

	}






	// BARANGAY ANNOUNCEMENT PAGE

	public function announcement($barangay_name, Request $request){
		$barangay_name = $request->barangay_name;


		$barangay_announcement_limit = DB::table('announcement')
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->select('announcement.*', 'barangay.barangay_name')
		->where('barangay.barangay_name', '=', $barangay_name)
		->orderBy('date_created', 'DESC')
		->limit(6)
		->get();

		$barangay_announcement_paginate = DB::table('announcement')
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->select('announcement.*', 'barangay.barangay_name', 'barangay.barangay_id')
		->where('barangay.barangay_name', '=', $barangay_name)
		->orderBy('date_created', 'DESC')
		->paginate(9);

		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();

		
		if($barangay_name == true){
			return view('front-page.announcement', compact('barangay', 'barangay_announcement_limit', 'barangay_announcement_paginate'));
		}else{
			return false;
		}

	}





	// BARANGAY ANNOUNCEMENT DETAILS PAGE


	public function announcementdetails($announcement_id, Request $request){
		$announcement_id = $request->announcement_id;

		$announcement_id = \Crypt::decrypt($request->announcement_id);
		$announcement = DB::table('announcement')->where('announcement_id', '=', $announcement_id)->first();

		$barangay = DB::table('barangay')->get();

		if($announcement_id == true){
			return view('front-page.announcement-details', compact('barangay','announcement'));
		}else{
			return false;
		}


	}






	// SEARCH ANNOUNCEMENT DATA WITH PAGINATION IN DYNAMIC

	public function searchpaginateannouncement($barangay_name, Request $request){

		$barangay_name = $request->barangay_name;
		$barangay = DB::table('barangay')->where('barangay_name', $barangay_name)->get();
		$q = $request->get('q');
		if($q != ""){
			$announcement_paginate = Announcement::where ( 'title', 'LIKE', '%' . $q . '%')
			->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
			->select('announcement.*', 'barangay.barangay_name')
			->where('barangay.barangay_name', '=', $barangay_name)
			->orWhere('announcement.date_created', 'LIKE', '%' .$q. '%')
			->paginate(6)
			->setPath ('');

			$pagination = $announcement_paginate->appends(array(
				'q' => $request->get('q')
			));
			if(count($announcement_paginate) > 0)

				return view('front-page.search-announcement', compact('barangay'))->withDetails($announcement_paginate)->withQuery($q);
		}

		if($barangay == true){
			return view('front-page.search-announcement', compact('barangay'))->withMessage ('No Details found. Try to search again!!!');
		}else{
			return false;
		}
	}



























}