<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


use App\Models\CityOfficial;
use App\Models\Barangay;
use App\Models\BarangayOfficial;
use App\Models\BarangayGallery;
use App\Models\Announcement;
use App\Models\BarangayWelcome;
use App\Models\BarangayVideo;
use App\User;
use Redirect;
use Response;
use Validate;
use Auth;
use DB;

class AdminController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}




	public function changepassworduser($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$sidebar = "dashboard";
		if($barangay_id == true){

			$barangay = DB::table('barangay')->where('user_id', Auth::user()->user_id)->get();
			// $barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->get();
			return view('admin.changepassword.changepassword', compact('sidebar','barangay'));

		}else{
			return false;
		}

	}



	public function changepassword(Request $request){
		if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
		}
		if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){  
		}
		$this->validate($request, [
			'current-password'  => 'required',
			'new-password'      => 'required|string|min:5|confirmed',
			
		]);
		$user = Auth::user();
		$user->password = bcrypt($request->get('new-password'));
		$user->save();

		$success = true;
		$message = "Your password has change successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}



	public function profile(Request $request){

		$user = Auth::user();
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->save();


		$success = true;
		$message = "Your data is updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);


	}



	// BARANGAY

	public function allbarangays(){

		$sidebar = "barangay";
		$barangays = DB::table('barangay')->where('user_id', Auth::user()->user_id)->get();
		// //dd($barangays);

		return view('admin.barangays.all', compact('sidebar', 'barangays'));
	}


	public function createbarangay(){
		$sidebar = "barangay";
		return view('admin.barangays.create', compact('sidebar'));
	}


	public function addbarangays(Request $request){

		$this->validate($request, [
			'display_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'

		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangays_images', $fileNameToStore);
		}
		else{
			$fileNameToStore = 'noimage.jpg';
		}

		$barangay = new Barangay;
		$barangay->barangay_name = $request->input('barangay_name');
		$barangay->display_image = $fileNameToStore;
		$barangay->description = $request->input('description');
		$barangay->user_id = $request->input('user_id');
		$barangay->save();


		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);


	}


	public function editbarangays($barangay_id, Request $request){

		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$sidebar = "barangay";
		if($barangay_id == true){

			$data = DB::table('barangay')->where('barangay_id', $barangay_id)->first();
			return view('admin.barangays.edit', compact('sidebar','data'));

		}else{
			return false;
		}


	}



	public function updatebarangaydata($id, Request $request){
		$this->validate($request, [
			'display_image' => 'nullable|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangays_images', $fileNameToStore);
		}
		$barangay_id = $request->barangay_id;
		$barangay = Barangay::find($barangay_id);
		$barangay->barangay_name = $request->input('barangay_name');
		$barangay->description = $request->input('description');
		if($request->hasFile('display_image'))
		{
			$barangay_id = $request->barangay_id;
			$barangaydata = Barangay::findorFail($barangay_id);
			$image_path = "public/storage/barangays_images/{$barangaydata->display_image}";
			unlink($image_path);
			$barangay->display_image = $fileNameToStore;
		}
		$barangay->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);


	}



	public function showviewbarangays($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$barangayofficials = DB::table('barangay_officials')
		->join('barangay', 'barangay_officials.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_officials.*', 'barangay.barangay_name')
		->where('barangay_officials.barangay_id', '=', $barangay_id)
		->get();

		$barangaygalleries = DB::table('barangay_galleries')
		->join('barangay', 'barangay_galleries.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_galleries.*', 'barangay.barangay_name')
		->where('barangay_galleries.barangay_id', '=', $barangay_id)
		->get();

		//dd($barangaygalleries);

		$sidebar = "dashboard";
		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();
		// 	if(count($barangay_id)){
			// dd($barangay);
			return view('admin.barangays.show', compact('sidebar', 'barangay', 'barangayofficials','barangaygalleries'));
		}
		else{
			return $barangay_id;
		}
		
	}



	public function deletebarangaydata($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay = Barangay::findorFail($barangay_id);
		$barangaydelete = Barangay::where('barangay_id', $barangay_id)->delete();
		$image_path = "public/storage/barangays_images/{$barangay->display_image}";

		if ($barangaydelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Barangay data deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Barangay data not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}





	// BARANGAY GALLERY


	public function viewbarangaygallery($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$barangaygalleries = DB::table('barangay_galleries')
		->join('barangay', 'barangay_galleries.barangay_id', '=', 'barangay.barangay_id')
		->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
		->select('barangay_galleries.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
		->where('barangay_galleries.barangay_id', '=', Auth::user()->user_id)
		->get();

		//dd($barangaygalleries);

		$sidebar = "dashboard";

		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();

			return view('admin.barangays.gallery.alldata', compact('sidebar','barangay' ,'barangaygalleries'));
		}
	}



	public function submitbarangaygallery(Request $request){
		$this->validate($request, [
			'display_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_gallery_images', $fileNameToStore);
		}
		else{
			$fileNameToStore = 'noimage.jpg';
		}

		$barangaygallery = new BarangayGallery;
		$barangaygallery->display_image = $fileNameToStore;
		$barangaygallery->date_taken = $request->input('date_taken');
		$barangaygallery->barangay_id = $request->input('barangay_id');
		$barangaygallery->save();


		$success = true;
		$message = "Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}


	public function deletebarangaygallerydata(Request $request){

		$barangay_gallery_id = $request->barangay_gallery_id;
		$barangaygallery = BarangayGallery::findorFail($barangay_gallery_id);
		$barangaygallerydelete = BarangayGallery::where('barangay_gallery_id', $barangay_gallery_id)->delete();
		$image_path = "public/storage/barangay_gallery_images/{$barangaygallery->display_image}";

		if ($barangaygallerydelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Barangay Image data deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Barangay Image data not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}





	// BARANGAY OFFICIAL

	public function addbarangayofficials(Request $request){

		$this->validate($request, [
			'display_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_officials_images', $fileNameToStore);
		}
		else{
			$fileNameToStore = 'noimage.jpg';
		}

		$barangayofficials = new BarangayOfficial;
		$barangayofficials->barangay_official_name = $request->input('barangay_official_name');
		$barangayofficials->position = $request->input('position');
		$barangayofficials->date_elected = $request->input('date_elected');
		$barangayofficials->end_term = $request->input('end_term');
		$barangayofficials->display_image = $fileNameToStore;
		$barangayofficials->description = $request->input('description');
		$barangayofficials->barangay_id = $request->input('barangay_id');
		$barangayofficials->save();

		$success = true;
		$message = "Barangay Official Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}	


	public function viewbarangayofficials($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$barangayofficials = DB::table('barangay_officials')
		->join('barangay', 'barangay_officials.barangay_id', '=', 'barangay.barangay_id')
		->select('barangay_officials.*', 'barangay.barangay_name')
		->where('barangay_officials.barangay_id', '=', $barangay_id)
		->get();

		$sidebar = "dashboard";

		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();

			

			return view('admin.barangays.barangayofficials.alldata', compact('sidebar','barangay' ,'barangayofficials'));
		}

	}


	public function editbarangayofficials($barangay_official_id, Request $request){
		$barangay_official_id = $request->barangay_official_id;


		$barangay_official_id = \Crypt::decrypt($request->barangay_official_id);

		$sidebar = "barangay";
		if($barangay_official_id == true){
			$data = DB::table('barangay_officials')->where('barangay_official_id', $barangay_official_id)->first();

			return view('admin.barangays.barangayofficials.edit', compact('sidebar','data'));
		}
		else{
			return $barangay_id;
		}


	}




	public function updatebarangayofficialdata($barangay_official_id, Request $request){

		$this->validate($request, [
			'display_image' => 'nullable|mimes:jpg,jpeg,png,bmp|max:20000'

		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_officials_images', $fileNameToStore);
		}

		$barangay_official_id = $request->barangay_official_id;
		$barangayofficial = BarangayOfficial::find($barangay_official_id);
		$barangayofficial->barangay_official_name = $request->input('barangay_official_name');
		$barangayofficial->position = $request->input('position');
		$barangayofficial->date_elected = $request->input('date_elected');
		$barangayofficial->end_term = $request->input('end_term');
		$barangayofficial->description = $request->input('description');

		if($request->hasFile('display_image'))
		{
			$barangay_official_id = $request->barangay_official_id;
			// $barangay_official_id = \Crypt::decrypt($request->barangay_official_id);

			$barangayofficialdata = BarangayOfficial::findorFail($barangay_official_id);
			$image_path = "public/storage/barangay_officials_images/{$barangayofficialdata->display_image}";
			unlink($image_path);
			$barangayofficial->display_image = $fileNameToStore;
		}

		$barangayofficial->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);


	}



	public function deletebarangayofficialdata($barangay_official_id, Request $request){
		$barangay_official_id = $request->barangay_official_id;
		$barangayofficial = BarangayOfficial::findorFail($barangay_official_id);
		$barangayofficialdelete = BarangayOfficial::where('barangay_official_id', $barangay_official_id)->delete();
		$image_path = "public/storage/barangay_officials_images/{$barangayofficial->display_image}";

		if ($barangayofficialdelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Barangay Official data deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Barangay Official data not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}




	// ANNOUNCEMENT

	public function addannouncement(Request $request){

		$this->validate($request, [
			//'display_image' =>  'required|image|'
			'display_image' => 'nullable|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/announcement_images', $fileNameToStore);
		}

		else{
			$fileNameToStore = 'noimage.jpg';
		}
		//var_dump($file);
		$announcement = new Announcement;
		$announcement->title = $request->input('title');
		$announcement->display_image = $fileNameToStore;
		$announcement->description = $request->input('description');
		$announcement->date_created = $request->input('date_created');
		$announcement->barangay_id = $request->input('barangay_id');
		$announcement->save();


		$success = true;
		$message = "Announcement Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}



	public function viewannouncement($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$announcements = DB::table('announcement')
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
		->select('announcement.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
		->where('announcement.barangay_id', '=', Auth::user()->user_id)
		->get();


		$announcements_json = DB::table('announcement')
		->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
		->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
		->select('announcement.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
		->where('announcement.barangay_id', '=', Auth::user()->user_id)
		->get();

		$testarrg = json_decode($announcements_json, true);
		// dd($announcements);

		$sidebar = "dashboard";

		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();

			return view('admin.barangays.announcement.alldata', compact('sidebar','barangay' ,'announcements', 'testarrg'));
		}
	}



	public function editannouncement($announcement_id, Request $request){
		$announcement_id = $request->announcement_id;
		$announcement_id = \Crypt::decrypt($request->announcement_id);

		$sidebar = "dashboard";
		if($announcement_id == true){
			$data = DB::table('announcement')->where('announcement_id', $announcement_id)->first();

			$barangay = DB::table('barangay')->get();

			return view('admin.barangays.announcement.edit', compact('sidebar','data', 'barangay'));
		}
		else{
			return false;
		}
	}


	public function updateannouncementdata($announcement_id, Request $request){

		$this->validate($request, [
			// 'display_image' =>  'nullable'

			'display_image' => 'nullable|mimes:jpg,jpeg,png,bmp|max:20000'

		]);
		if($request->hasFile('display_image')){

			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/announcement_images', $fileNameToStore);
		}

		$announcement_id = $request->announcement_id;
		$announcement = Announcement::find($announcement_id);
		$announcement->title = $request->input('title');
		$announcement->description = $request->input('description');
		$announcement->date_created = $request->input('date_created');


		if($request->hasFile('display_image') == 'noimage.jpg')
		{
			// $announcement_id = $request->announcement_id;
			// $announcementdata = Announcement::findorFail($announcement_id);
			// $image_path = "public/storage/announcement_images/{$announcementdata->display_image}";
			// unlink($image_path);
			$announcement->display_image = $fileNameToStore;
		}
		else{
			// $fileNameToStore = 'noimage.jpg';
			$announcement_id = $request->announcement_id;
			$announcementdata = Announcement::findorFail($announcement_id);
			$image_path = "public/storage/announcement_images/{$announcementdata->display_image}";
			unlink($image_path);
			$announcement->display_image = $fileNameToStore;
		}


		$announcement->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}



	public function deleteannouncementdata($announcement_id, Request $request){

		$announcement_id = $request->announcement_id;
		$announcement = Announcement::findorFail($announcement_id);
		$announcementdelete = Announcement::where('announcement_id', $announcement_id)->delete();
		$image_path = "public/storage/announcement_images/{$announcement->display_image}";

		if ($announcementdelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Announcement data deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Announcement data not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}




	// BARANGAY VIDEO


	public function viewbarangayvideo($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$barangayvideo = DB::table('barangay_video')
		->join('barangay', 'barangay_video.barangay_id', '=', 'barangay.barangay_id')
		->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
		->select('barangay_video.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
		->where('barangay_video.barangay_id', '=', Auth::user()->user_id)
		->get();

		$sidebar = "dashboard";

		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();

			return view('admin.barangays.barangayvideo.alldata', compact('sidebar','barangay' ,'barangayvideo'));
		}
	}




	public function addbarangayvideo(Request $request){

		$barangayvideo = new BarangayVideo;
		$barangayvideo->embed_video = $request->input('embed_video');
		$barangayvideo->barangay_id = $request->input('barangay_id');
		$barangayvideo->save();


		$success = true;
		$message = "Video Added successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}




	public function deletebarangayvideo($barangay_video_id, Request $request){	
		$barangay_video_id = $request->barangay_video_id;
		$barangayvideodata = BarangayVideo::where('barangay_video_id', $barangay_video_id)->delete();
		if ($barangayvideodata == 1) 
		{
			$success = true;
			$message = "Video deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = "Video not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}





	// BARANGAY WELCOME

	public function addbarangaywelcome(Request $request){

		$this->validate($request, [
			'display_image' => 'required|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image')){
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_welcome_images', $fileNameToStore);
		}
		else{
			$fileNameToStore = 'noimage.jpg';
		}

		$barangaywelcome = new BarangayWelcome;
		$barangaywelcome->title = $request->input('title');
		$barangaywelcome->display_image = $fileNameToStore;
		$barangaywelcome->description = $request->input('description');
		$barangaywelcome->about_description = $request->input('about_description');
		$barangaywelcome->barangay_id = $request->input('barangay_id');
		$barangaywelcome->save();


		$success = true;
		$message = "Data Added Successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);

	}




	public function editbarangaywelcome($barangay_welcome_id, Request $request){
		$barangay_welcome_id = $request->barangay_welcome_id;
		$barangay_welcome_id = \Crypt::decrypt($request->barangay_welcome_id);


		$sidebar = "dashboard";
		if($barangay_welcome_id == true){
			$data = DB::table('barangay_welcome')->where('barangay_welcome_id', $barangay_welcome_id)->first();

			return view('admin.barangays.barangaywelcome.edit', compact('sidebar','data'));
		}
		else{
			return false;
		}
	}





	public function updatebarangaywelcomedata($barangay_welcome_id, Request $request){

		$this->validate($request, [
			'display_image' => 'nullable|mimes:jpg,jpeg,png,bmp|max:20000'
		]);
		if($request->hasFile('display_image'))
		{
			$fileNameWithExt = $request->file('display_image')->getClientOriginalName();
			$filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('display_image')->getClientOriginalExtension();
			$fileNameToStore = $filename.'_'.time().'.'.$extension;
			$path = $request->file('display_image')->storeAs('public/barangay_welcome_images', $fileNameToStore);
		}

		$barangay_welcome_id = $request->barangay_welcome_id;
		$barangaywelcome = BarangayWelcome::find($barangay_welcome_id);
		$barangaywelcome->title = $request->input('title');
		$barangaywelcome->description = $request->input('description');
		$barangaywelcome->about_description = $request->input('about_description');
		
		if($request->hasFile('display_image')){
			$barangay_welcome_id = $request->barangay_welcome_id;
			$barangaywelcomedata = BarangayWelcome::findorFail($barangay_welcome_id);
			$image_path = "public/storage/barangay_welcome_images/{$barangaywelcomedata->display_image}";
			unlink($image_path);
			$barangaywelcome->display_image = $fileNameToStore;
		}
		$barangaywelcome->save();

		$success = true;
		$message = "Updated successfully";
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}







	public function viewbarangaywelcome($barangay_id, Request $request){
		$barangay_id = $request->barangay_id;
		$barangay_id = \Crypt::decrypt($request->barangay_id);

		$barangaywelcome = DB::table('barangay_welcome')
		->join('barangay', 'barangay_welcome.barangay_id', '=', 'barangay.barangay_id')
		->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
		->select('barangay_welcome.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
		->where('barangay_welcome.barangay_id', '=', Auth::user()->user_id)
		->first();

		$sidebar = "dashboard";

		if($barangay_id == true){
			$barangay = DB::table('barangay')->where('barangay_id', $barangay_id)->first();

			return view('admin.barangays.barangaywelcome.alldata', compact('sidebar','barangay' ,'barangaywelcome'));
		}
	}




	public function deletebarangaywelcomedata($barangay_welcome_id, Request $request){

		$barangay_welcome_id = $request->barangay_welcome_id;
		$barangaywelcome = BarangayWelcome::findorFail($barangay_welcome_id);
		$barangaywelcomedelete = BarangayWelcome::where('barangay_welcome_id', $barangay_welcome_id)->delete();
		$image_path = "public/storage/barangay_welcome_images/{$barangaywelcome->display_image}";

		if ($barangaywelcomedelete == 1) 
		{
			unlink($image_path);
			$success = true;
			$message = "Data data deleted successfully";
		} 
		else 
		{
			$success = false;
			$message = " Data not found";
		}
		return response()->json([
			'success' => $success,
			'message' => $message,
		]);
	}








}
