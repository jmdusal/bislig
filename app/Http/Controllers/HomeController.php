<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }




    public function index(){
        return redirect('/dashboard');
    }





    public function admin(Request $request){
        // $barangay_name = $request->barangay_name;

        // $barangay_id = $request->barangay_id;
        // $barangay_id = \Crypt::decrypt($request->barangay_id);

        $barangays = DB::table('barangay')
        ->join('users', 'barangay.user_id', '=', 'users.user_id')
        ->select('barangay.*', 'users.user_id')
        ->where('barangay.user_id', '=', Auth::user()->user_id)
        ->get();

        
        $barangayofficials = DB::table('barangay_officials')
        ->join('barangay', 'barangay_officials.barangay_id', '=', 'barangay.barangay_id')
        ->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
        ->select('barangay_officials.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
        ->where('barangay_officials.barangay_id', '=', Auth::user()->user_id)
        ->get();


        $announcements = DB::table('announcement')
        ->join('barangay', 'announcement.barangay_id', '=', 'barangay.barangay_id')
        ->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
        ->select('announcement.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
        ->where('announcement.barangay_id', '=', Auth::user()->user_id)
        ->get();


        $barangaygalleries = DB::table('barangay_galleries')
        ->join('barangay', 'barangay_galleries.barangay_id', '=', 'barangay.barangay_id')
        ->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
        ->select('barangay_galleries.*', 'barangay.barangay_name', 'users.user_id', 'users.name')
        ->where('barangay_galleries.barangay_id', '=', Auth::user()->user_id)
        ->get();


        $barangaywelcome = DB::table('barangay_welcome')
        ->join('barangay', 'barangay_welcome.barangay_id', '=', 'barangay.barangay_id')
        ->leftJoin('users as users', 'barangay.user_id', '=', 'users.user_id')
        ->select('barangay_welcome.*', 'barangay.barangay_name','barangay.barangay_id', 'users.user_id', 'users.name')
        ->where('barangay_welcome.barangay_id', '=', Auth::user()->user_id)
        ->get();


        $barangay = DB::table('barangay')
        ->join('users', 'barangay.user_id', '=', 'users.user_id')
        ->select('barangay.*', 'users.user_id')
        ->where('barangay.user_id', '=', Auth::user()->user_id)
        ->get();

        $sidebar = "dashboard";
        return view('admin.dashboard_admin', compact('sidebar', 'barangays', 'barangayofficials', 'announcements','barangaygalleries', 'barangaywelcome', 'barangay'));
        // return view('admin.dashboard_admin')->with('barangays', $barangays)->with('barangayofficials', $barangayofficials)->with('sidebar', $sidebar);
        
    }



}
