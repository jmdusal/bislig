<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth; 

// Route::get('/', function () {
// 	return view('welcome');
// });





Route::group(['middleware' => 'disablepreventback'],function(){
	Auth::routes();
	Route::get('/dashboard', 'HomeController@admin');
	// Route::get('/admin-dashboard', 'HomeController@admin')->middleware(['auth']);
	
	Route::get('/logout', 'Auth\LoginController@logout');


	Route::get('/{barangay_name}', 'FrontPageController@homepage');
	// CITY OFFICIAL ROUTES
	// Route::get('cityofficials/table', 'AdminController@allcityofficials');
	// Route::get('cityofficial/add', 'AdminController@createcityofficial');
	// Route::post('/cityofficials/submit', 'AdminController@addcityofficials');
	// Route::get('/cityofficial/edit/{city_official_id}', 'AdminController@editcityofficials');
	// Route::get('/cityofficial/profile/data/{city_official_id}', 'AdminController@showviewcityofficials');
	// Route::patch('/cityofficial/profile/update/{city_official_id}', 'AdminController@editcityofficialprofiledata');
	// Route::patch('/cityofficial/{city_official_id}', 'AdminController@updateofficialdata');
	// Route::post('/deletecityofficial/{city_official_id}', 'AdminController@deletecityofficialdata');



	// BARANGAY ROUTES
	Route::get('barangay/table', 'AdminController@allbarangays');
	Route::get('barangay/add', 'AdminController@createbarangay');
	Route::post('/barangays/submit', 'AdminController@addbarangays');
	Route::get('/barangay/edit/{barangay_id}', 'AdminController@editbarangays');
	Route::get('/barangay/data/{barangay_id}', 'AdminController@showviewbarangays');
	Route::patch('/barangay/profile/update/{barangay_id}', 'AdminController@editbarangayprofiledata');
	Route::patch('/barangay/{barangay_id}', 'AdminController@updatebarangaydata');
	Route::post('/deletebarangay/{barangay_id}', 'AdminController@deletebarangaydata');



	// BARANGAY OFFICIALS ROUTES
	Route::post('/barangayofficials/submit', 'AdminController@addbarangayofficials');
	Route::get('/barangayofficial/edit/{barangay_official_id}', 'AdminController@editbarangayofficials');
	Route::patch('/barangayofficial/{barangay_official_id}', 'AdminController@updatebarangayofficialdata');
	Route::post('/deletebarangayofficial/{barangay_official_id}', 'AdminController@deletebarangayofficialdata');
	Route::get('/barangay/officials/{barangay_id}', 'AdminController@viewbarangayofficials');





	// BARANGAY GALLERY
	Route::post('/barangaygallery/submit', 'AdminController@submitbarangaygallery');
	Route::post('/deletebarangaygallery/{barangay_gallery_id}', 'AdminController@deletebarangaygallerydata');
	Route::get('/barangay-galleries/{barangay_id}', 'AdminController@viewbarangaygallery');


	
	

	// ANNOUNCEMENT
	Route::post('/announcement/submit', 'AdminController@addannouncement');
	Route::get('/announcement/edit/{announcement_id}', 'AdminController@editannouncement');
	Route::patch('/announcement/{announcement_id}', 'AdminController@updateannouncementdata');
	Route::post('/deleteannouncement/{announcement_id}', 'AdminController@deleteannouncementdata');
	Route::get('/announcement/{barangay_id}', 'AdminController@viewannouncement');




	// BARANGAY VIDEO
	Route::post('/barangayvideo/submit', 'AdminController@addbarangayvideo');
	Route::post('/deletebarangayvideo/{barangay_video_id}', 'AdminController@deletebarangayvideo');
	Route::get('/barangay-video/{barangay_id}', 'AdminController@viewbarangayvideo');




	// BARANGAY WELCOME
	Route::post('/barangaywelcome/submit', 'AdminController@addbarangaywelcome');
	Route::get('/barangaywelcome/edit/{barangay_welcome_id}', 'AdminController@editbarangaywelcome');
	Route::patch('/barangaywelcome/{barangay_welcome_id}', 'AdminController@updatebarangaywelcomedata');
	Route::post('/deletebarangaywelcome/{barangay_welcome_id}', 'AdminController@deletebarangaywelcomedata');
	Route::get('/barangay-welcome/{barangay_id}', 'AdminController@viewbarangaywelcome');




	Route::post('/profile/change-data', 'AdminController@profile');


	Route::get('/change-password/{barangay_id}', 'AdminController@changepassworduser');
	Route::post('/change-password', 'AdminController@changepassword')->name('changePassword');
});






Route::get('/barangay-officials/{barangay_name}', 'FrontPageController@barangayofficials');


Route::get('/barangay/announcement/{barangay_name}', 'FrontPageController@announcement');

Route::get('/announcement-details/{barangay_name}/{announcement_id}', 'FrontPageController@announcementdetails');

Route::any('/announcement/search/{barangay_name}', 'FrontPageController@searchpaginateannouncement');







Route::get('/barangay-gallery/{barangay_name}', 'FrontPageController@gallery');

Route::get('/barangay-videos/{barangay_name}', 'FrontPageController@video');

Route::get('/about-barangay/{barangay_name}', 'FrontPageController@about');
