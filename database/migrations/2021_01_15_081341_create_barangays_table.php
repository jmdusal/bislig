<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangaysTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('barangays')){
            Schema::create('barangays', function (Blueprint $table) {
                $table->bigIncrements('barangay_id');
                $table->string('barangay_name');
                $table->string('display_image');
                $table->longtext('description');
                $table->biginteger('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('user_id')->on('users')->onDelete('CASCADE');
                
            });
        }
    }

    
    public function down()
    {
        Schema::dropIfExists('barangays');
    }
}
