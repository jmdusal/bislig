<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateElectedToBarangayOfficials extends Migration
{

    public function up()
    {
        if(!Schema::hasColumn('date_elected')){
            Schema::table('barangay_officials', function (Blueprint $table) {
                $table->string('date_elected')->after('position');
            });
        }
    }

    
    public function down()
    {
        Schema::table('barangay_officials', function (Blueprint $table) {
            //
        });
    }
}
