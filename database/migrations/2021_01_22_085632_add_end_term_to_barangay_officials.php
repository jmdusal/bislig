<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndTermToBarangayOfficials extends Migration
{
    
    public function up()
    {
        if(!Schema::hasColumn('end_term')){
            Schema::table('barangay_officials', function (Blueprint $table) {
                $table->string('end_term')->after('date_elected');
            });
        }
    }

    
    public function down()
    {
        Schema::table('barangay_officials', function (Blueprint $table) {
            //
        });
    }
}
