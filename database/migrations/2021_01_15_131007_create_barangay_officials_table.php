<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayOfficialsTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('barangay_officials')){
            Schema::create('barangay_officials', function (Blueprint $table) {
                $table->bigIncrements('barangay_official_id');
                $table->string('barangay_official_name');
                $table->string('position');
                $table->string('display_image');
                $table->biginteger('barangay_id')->unsigned()->nullable();
                $table->foreign('barangay_id')->references('barangay_id')->on('barangays')->onDelete('CASCADE');
            });
        }
    }

    
    public function down()
    {
        Schema::dropIfExists('barangay_officials');
    }
}
