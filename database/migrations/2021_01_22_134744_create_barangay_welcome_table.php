<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayWelcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('barangay_welcome')){
            Schema::create('barangay_welcome', function (Blueprint $table) {
                $table->bigIncrements('barangay_welcome_id');
                $table->string('title');
                $table->string('display_image');
                $table->longtext('description');
                $table->biginteger('barangay_id')->unsigned()->nullable();
                $table->foreign('barangay_id')->references('barangay_id')->on('barangays')->onDelete('CASCADE');
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangay_welcome');
    }
}
