<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateTakenToBarangayGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('date_taken')){
            Schema::table('barangay_galleries', function (Blueprint $table) {
                $table->string('date_taken')->after('display_image');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barangay_galleries', function (Blueprint $table) {
            //
        });
    }
}
