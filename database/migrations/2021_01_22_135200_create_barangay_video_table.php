<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('barangay_video')){
            Schema::create('barangay_video', function (Blueprint $table) {
                $table->bigIncrements('barangay_video_id');
                $table->longtext('embed_video');
                $table->biginteger('barangay_id')->unsigned()->nullable();
                $table->foreign('barangay_id')->references('barangay_id')->on('barangays')->onDelete('CASCADE');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangay_video');
    }
}
