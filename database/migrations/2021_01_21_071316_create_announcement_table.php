<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('announcement')){
            Schema::create('announcement', function (Blueprint $table) {
                $table->bigIncrements('announcement_id');
                $table->string('title');
                $table->string('display_image');
                $table->longtext('description');
                $table->string('date_created');
                $table->biginteger('barangay_id')->unsigned()->nullable();
                $table->foreign('barangay_id')->references('barangay_id')->on('barangays')->onDelete('CASCADE');

            });
        }
    }

    
    public function down()
    {
        Schema::dropIfExists('announcement');
    }
}
