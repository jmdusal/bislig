<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangayGalleriesTable extends Migration
{

    public function up()
    {
        if(!Schema::hasTable('barangay_galleries')){
            Schema::create('barangay_galleries', function (Blueprint $table) {
                $table->bigIncrements('barangay_gallery_id');
                $table->string('display_image');
                $table->biginteger('barangay_id')->unsigned()->nullable();
                $table->foreign('barangay_id')->references('barangay_id')->on('barangays')->onDelete('CASCADE');
            });
        }
    }

    
    public function down()
    {
        Schema::dropIfExists('barangay_galleries');
    }
}
