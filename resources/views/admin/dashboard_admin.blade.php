@extends('admin.header_admin')

@section('title')
Dashboard
@endsection


@section('content')

@if(count($barangays) != 0)
<div class="pcoded-content">
	<div class="pcoded-inner-content">
		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							
						</div>
						
					</div>
				</div>

				
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div>
								<div class="content social-timeline">
									<div class="">

										<div class="row">
											<div class="col-md-12">

												<div class="social-wallpaper">

													
													<img src="{{ asset('adminty//files/assets/images/social/img1.jpg')}}" class="img-fluid width-100" alt="" />
													
												</div>

											</div>
										</div>


										<div class="row">
											<div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">

												<div class="social-timeline-left">


													@foreach($barangays as $barangay)
													<div class="card">
														<div class="social-profile">

															<a href="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}" data-lightbox="gallery">
																<img class="img-fluid width-100" style="object-fit: cover; height: 200px; width: 100%;" src="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}" alt="">
															</a>
														</div>
														<div class="card-block social-follower">
															<h4>Brgy. {{$barangay->barangay_name}}</h4>
															<!-- <h5>{!!$barangay->description!!}</h5> -->
															<div class="row follower-counter">
																
															</div>



															<div class="">

																<a type="button" href="{{url('/barangay/officials')}}/{{\Crypt::encrypt($barangay->barangay_id)}}" class="btn btn-outline-success waves-effect btn-block"><i class="icofont icofont-eye-alt"></i>&nbsp; View Barangay Officials</a>



																<a type="button" href="{{url('/announcement')}}/{{\Crypt::encrypt($barangay->barangay_id)}}" class="btn btn-outline-success waves-effect btn-block"><i class="icofont icofont-plus-circle"></i>&nbsp; Post Announcement</a>


																<a type="button" href="{{url('/barangay-galleries')}}/{{\Crypt::encrypt($barangay->barangay_id)}}" class="btn btn-outline-success waves-effect btn-block"><i class="icofont icofont-plus-circle"></i>&nbsp; Add Image in Gallery</a>



																<a type="button" href="{{url('/barangay-video')}}/{{\Crypt::encrypt($barangay->barangay_id)}}" class="btn btn-outline-success waves-effect btn-block"><i class="icofont icofont-table"></i>&nbsp; Video Table</a>


																<!-- <a type="button" href="{{url('/barangay-welcome')}}/{{\Crypt::encrypt($barangay->barangay_id)}}" class="btn btn-outline-success waves-effect btn-block"><i class="icofont icofont-table"></i>&nbsp; Welcome Table</a> -->



																<button type="button" class="btn btn-outline-success waves-effect btn-block" data-toggle="modal" data-target="#modal1"><i class="icofont icofont-eye-alt"></i>&nbsp; Barangay Welcome</button>


																

															</div>

															<br>
															<a href="{{url('/', $barangay->barangay_name)}}">Go to Site</a>

														</div>
													</div>
													@endforeach




												</div>

											</div>
											<div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

												<div class="card social-tabs">
													<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
														<li class="nav-item">
															<a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">Timeline</a>
															<div class="slide"></div>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#about" role="tab">About</a>
															<div class="slide"></div>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#photos" role="tab">Gallery</a>
															<div class="slide"></div>
														</li>


														<!-- <li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#friends" role="tab">Friends</a>
															<div class="slide"></div>
														</li> -->



													</ul>
												</div>








												<div class="tab-content">

													<div class="tab-pane active" id="timeline">
														<div class="row">
															<div class="col-md-12 timeline-dot">
																<div class="social-timelines p-relative">
																	<div class="row timeline-right p-t-35">
																		<div class="col-2 col-sm-2 col-xl-1">
																			<div class="social-timelines-left">

																				@foreach($barangays as $barangay)

																				<img class="img-radius timeline-icon" src="{{ asset('storage/barangays_images')}}/{{$barangay->display_image}}" alt="">

																				@endforeach
																			</div>
																		</div>

																		
																		<div class="col-10 col-sm-10 col-xl-11 p-l-5 p-b-35">

																			@foreach($announcements as $data)
																			<div class="card">

																				<div class="card-block post-timelines">

																					<!-- <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip"></span>
																					
																					<div class="dropdown-menu dropdown-menu-right b-none services-list">
																						<a class="dropdown-item" href="#">Remove tag</a>
																						<a class="dropdown-item" href="#">Report Photo</a>
																						<a class="dropdown-item" href="#">Hide From Timeline</a>
																						<a class="dropdown-item" href="#">Blog User</a>
																					</div> -->

																					<div class="chat-header f-w-600">Brgy. {{$data->barangay_name}} posted an Announcement</div>
																					<div class="social-time text-muted">
																						<?php
																						$orig_date =  explode('-', $data->date_created);
																						$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
																						echo date("M d, Y", strtotime($con_date));
																						?>
																						
																					</div>
																				</div>

																				<a href="{{ asset('storage/announcement_images')}}/{{$data->display_image}}" data-lightbox="gallery">

																					<img style="object-fit: cover; height: 350px; width: 10s0%;" src="{{ asset('storage/announcement_images')}}/{{$data->display_image}}" class="img-fluid width-100" alt="" data-lightbox="gallery">

																				</a>

																				<div class="card-block">
																					<div class="timeline-details">


																						<!-- <div class="chat-header">Josephin Doe posted on your timeline</div>
																						-->

																						<p class="text-muted">{!!$data->description!!}</p>
																					</div>
																				</div>
																				
																				
																			</div>
																			@endforeach
																		</div>
																		

																	</div>
																</div>
																
															</div>
														</div>

													</div>












													<div class="tab-pane" id="about">
														<div class="row">


															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Barangay Information</h5>
																		<button id="edit-btn" type="button" class="btn btn-success waves-effect waves-light f-right">
																			<i class="icofont icofont-edit"></i>
																		</button>
																	</div>
																	<div class="card-block">
																		<div id="view-info" class="row">
																			<div class="col-lg-6 col-md-12">
																				@foreach($barangays as $barangay)
																				<form>

																					<table class="table table-responsive m-b-0">


																						<tr>
																							<th class="social-label b-none p-t-0">Barangay Name
																							</th>
																							<td class="social-user-name b-none p-t-0 text-muted">{{$barangay->barangay_name}}</td>
																						</tr>
																						

																						<tr>
																							<th class="social-label b-none">Description</th>
																							<td class="social-user-name b-none text-muted">{!!$barangay->description!!}</td>
																						</tr>


																						
																					</table>
																				</form>

																				@endforeach
																			</div>
																		</div>




																		<div id="edit-info" class="row">
																			<div class="col-lg-12 col-md-12">

																				@foreach($barangays as $data)
																				<form method="post" action="{{url('barangay', $data->barangay_id)}}" enctype="multipart/form-data" class="form_submit">
																					{{csrf_field()}}

																					<input type="hidden" name="_method" value="PATCH">


																					<div class="input-group">
																						<input type="text" name="barangay_name" class="form-control" value="{{$data->barangay_name}}" placeholder="Full Name">
																					</div>
																					
																					<div class="input-group">
																						
																						<input type="file" name="display_image" class="form-control">
																						<br>
																						<img src="{{ asset('storage/barangays_images')}}/{{$data->display_image}}" class="img-thumbnail" width="100" />

																						<input type="hidden" name="hidden_image" value="{{ $data->display_image}}" />
																					</div>
																					
																					<div class="md-group-add-on">
																						<textarea name="description" rows="5" cols="5" class="form-control" placeholder="Address..." >{!!$data->description!!}</textarea>
																					</div>
																					<div class="text-center m-t-20">




																						<input type="submit" class="btn btn-success waves-effect waves-light m-r-20" value="Save">

																						<a href="javascript:;" id="edit-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>
																					</div>

																				</form>
																				@endforeach

																			</div>
																		</div>



																	</div>
																</div>
															</div>











															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		<h5 class="card-header-text">Admin Information</h5>
																		<button id="edit-Contact" type="button" class="btn btn-success waves-effect waves-light f-right">
																			<i class="icofont icofont-edit"></i>
																		</button>
																	</div>
																	<div class="card-block">
																		<div id="contact-info" class="row">
																			<div class="col-lg-6 col-md-12">
																				<table class="table table-responsive m-b-0">
																					
																					<tr>
																						<th class="social-label b-none">Name</th>
																						<td class="social-user-name b-none text-muted">{{Auth::user()->name}}</td>
																					</tr>



																					<tr>
																						<th class="social-label b-none">E-mail </th>
																						<td class="social-user-name b-none text-muted">{{Auth::user()->email}}</td>
																					</tr>



																				</table>
																			</div>
																		</div>
																		<div id="edit-contact-info" class="row">
																			<div class="col-lg-12 col-md-12">

																				<form method="post" action="{{url('/profile/change-data')}}" enctype="multipart/form-data" class="form_submit_user">
																					{{csrf_field()}}

																					<div class="input-group">
																						<input type="text" name="name" class="form-control" value="{{Auth::user()->name}}" placeholder="Enter Name">
																					</div>
																					<div class="input-group">
																						<input type="text" name="email" class="form-control" value="{{Auth::user()->email}}" placeholder="Enter Email address">
																					</div>
																					
																					
																					<div class="text-center m-t-20">
																						<!-- <a href="javascript:;" id="contact-save" class="btn btn-primary waves-effect waves-light m-r-20">Save</a> -->

																						<input type="submit" class="btn btn-success waves-effect waves-light m-r-20" value="Save">


																						<a href="javascript:;" id="contact-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>



																					</div>


																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>






















															<div class="col-sm-12">
																<div class="card">
																	<div class="card-header">
																		
																		<button id="edit-work" type="button" class="btn btn-success waves-effect waves-light f-right">
																			<i class="icofont icofont-edit">&emsp;Change password</i>
																		</button>
																	</div>

																	<div class="card-block">
																		
																		<div id="edit-contact-work" class="row">
																			<div class="col-lg-12 col-md-12">

																				<form method="post" action="{{ route('changePassword') }}" class="form_submit_changepassword">
																					{{csrf_field()}}


																					<div class="form-group row">
																						<label class="col-sm-2 col-form-label">Current Password</label>
																						<div class="col-sm-5">
																							<input id="current-password" type="password" class="form-control" name="current-password" required>
																						</div>
																					</div>




																					<div class="form-group row">
																						<label class="col-sm-2 col-form-label">New Password</label>
																						<div class="col-sm-5">
																							<input id="new-password" type="password" class="form-control" name="new-password" required>
																						</div>
																					</div>



																					<div class="form-group row">
																						<label class="col-sm-2 col-form-label">Confirm New Password</label>
																						<div class="col-sm-5">
																							<input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
																						</div>
																					</div>

																					<br>
																					<input type="checkbox" onclick="myFunctionshowpassword()">&nbsp;Show Password
																					<br><br><br>

																					

																					<div class="text-center m-t-20">

																						<input type="submit" class="btn btn-success waves-effect waves-light m-r-20" value="Save">

																						<a href="javascript:;" id="work-cancel" class="btn btn-default waves-effect waves-light">Cancel</a>
																					</div>


																				</form>

																			</div>
																		</div>
																	</div>
																</div>
															</div>

														</div>
													</div>










													<div class="tab-pane" id="photos">
														<div class="card">
															<div class="card-block">

																@foreach($barangaygalleries as $data)
																<div class="col-md-4" style="padding: 6px">

																	<!-- <ul id="profile-lightgallery" class="col-md-4"> -->

																		<div class="container" style="width: 100%; padding: 0px">

																			<a href="{{ asset('storage/barangay_gallery_images')}}/{{$data->display_image}}" data-lightbox="gallery">
																				<img src="{{ asset('storage/barangay_gallery_images')}}/{{$data->display_image}}" class="img-thumbnail" alt="Avatar">
																			</a>

																		</div>


																		<!-- </ul> -->

																	</div>
																	@endforeach


																</div>

															</div>
														</div>





													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			@else


			@endif

			<form role="form" method="post" action="{{url('/barangaywelcome', $barangaywelcome[0]->barangay_welcome_id)}}" enctype="multipart/form-data" class="form_submit_barangaywelcome">
				{{csrf_field()}}
				<input type="hidden" name="_method" value="PATCH">


				<div class="modal fade modal-icon" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg card business-info services m-b-20" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<center>
									<h4 class="modal-title" id="myModalLabel">Barangay Welcome</h4>
								</center>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">


								<div class="col-md-12 text-center">
									<div class="icon-list-demo">
									</div>
								</div>
								<div class="col-md-12">

									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Title</label>
										<div class="col-sm-10">
											<input type="text" name="title" class="form-control" value="{{$barangaywelcome[0]->title}}" required="true">
										</div>
									</div>


									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Upload Image</label>
										<div class="col-sm-10">
											<input type="file" name="display_image" class="form-control">
											<img src="{{ asset('storage/barangay_welcome_images')}}/{{$barangaywelcome[0]->display_image}}" class="img-thumbnail" width="100" />
											<input type="hidden" name="hidden_image" value="{{ $barangaywelcome[0]->display_image}}" />
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Description</label>
										<div class="col-sm-10">
											<textarea name="description" rows="12" cols="7" class="form-control">{{$barangaywelcome[0]->description}}</textarea>
										</div>
									</div>

									<div class="form-group row">
										<label class="col-sm-2 col-form-label">About Description</label>
										<div class="col-sm-10">
											<textarea name="about_description" rows="12" cols="7" class="form-control">{{$barangaywelcome[0]->about_description}}</textarea>
										</div>
									</div>


								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="create" class="btn btn-success mr-3"><i class="feather icon-save"></i>&nbsp;Save</button>
							<button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
						</div>
					</form>

					<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

					<script type="text/javascript">
	// new Splide( '.splide', {
	// 	type  : 'fade',
	// 	rewind: true,
	// } ).mount();




	$(".form_submit").submit(function(e) {
		e.preventDefault();
		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>',
			allowOutsideClick: false,
			showConfirmButton:false,
		});
		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){
			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		setTimeout(function(){
      			window.location.href = '{{ url("/dashboard")}}';
      		}, 1500);

      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		location.reload(true);
      	}
      },
      resetForm: true
  });

		}, 1500);
	});

	$(".form_submit_user").submit(function(e) {
		e.preventDefault();
		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>',
			allowOutsideClick: false,
			showConfirmButton:false,
		});
		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){
			$('.form_submit_user').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		setTimeout(function(){
      			window.location.href = '{{ url("/dashboard")}}';
      		}, 1500);

      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		location.reload(true);
      	}
      },
      resetForm: true
  });

		}, 1500);
	});


	$(".form_submit_barangaywelcome").submit(function(e) {
		$('#modal1').modal('hide');

		e.preventDefault();

		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit_barangaywelcome').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){

					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);
          
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		
      		setTimeout(function(){
      			window.location.href = '{{ url("/dashboard")}}';
      		}, 1500);
      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1500);
      	}

      },
      resetForm: true
  });
		}, 1500);
	});


	$(".form_submit_changepassword").submit(function(e) {
		e.preventDefault();
		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit_changepassword').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
          	title: "Error!",
          	text: "Please check some input fields!",
          	type: "error",
          	showConfirmButton: true
          });
          // location.reload(true);
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);

      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		setTimeout(function(){
      			window.location.href = '{{ url("/dashboard")}}';
      		}, 1500);
      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1500);
      	}
      },
      resetForm: true
  });

		}, 1500);
	});


	function myFunctionshowpassword() {
		var x = document.getElementById("current-password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}

		var y = document.getElementById("new-password");
		if (y.type === "password") {
			y.type = "text";
		} else {
			y.type = "password";
		}

		var z = document.getElementById("new-password-confirm");
		if (z.type === "password") {
			z.type = "text";
		} else {
			z.type = "password";
		}
	}

	document.addEventListener( 'DOMContentLoaded', function () {
		var secondarySlider = new Splide( '#secondary-slider', {
			fixedWidth  : 100,
			height      : 60,
			gap         : 10,
			cover       : true,
			isNavigation: true,
			focus       : 'center',
			breakpoints : {
				'600': {
					fixedWidth: 66,
					height    : 40,
				}
			},
		} ).mount();

		var primarySlider = new Splide( '#primary-slider', {
			type       : 'fade',
			heightRatio: 0.5,
			pagination : false,
			arrows     : false,
			cover      : true,
	} ); // do not call mount() here.

		primarySlider.sync( secondarySlider ).mount();
	} );
</script>

@endsection