@extends('admin.header_admin')

@section('title')
Barangay {{$barangay->barangay_name}} Welcome 
@endsection

@section('stylesheet')
<style type="text/css">
	.table td{
		max-width: 200px; 
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;

	}
</style>
<script>
	tinymce.init({

		selector: 'textarea',
		height: 200,
		menubar: false,
		plugins: [

		'advlist autolink lists link image charmap print preview anchor',
		'searchreplace visualblocks code fullscreen',
		'insertdatetime media table paste code help wordcount'
		],

	});
</script>

@endsection
@section('content')

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">
									

									<!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Data</button> -->

									
								</div>
							</div>
						</div>
						
					</div>
				</div>


				




















				<div class="page-body">
					<div class="card">
						<div class="card-header">

							
							<center>
								<h4>Barangay Welcome Table</h4>
							</center>
							
							
							

						</div>
						<div class="card-block">
							<div class="dt-responsive table-responsive">
								<table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
									<thead>
										<tr>
											<th width="1%">Image</th>
											<th>Title </th> 
											<th>Description</th>
											<th>About Description</th>


											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>

										@foreach($barangaywelcome as $data)
										<tr>
											<td>
												<a href="{{ asset('storage/barangay_welcome_images')}}/{{$data->display_image}}" data-lightbox="gallery">
													<img src="{{ asset('storage/barangay_welcome_images')}}/{{$data->display_image}}" class="img-thumbnail" width="200">
												</a>
											</td>
											<td>{{$data->title}}</td>
											<td>{!!$data->description!!}</td>
											<td>{!!$data->about_description!!}</td>

											<td class="text-center">
												<button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!-- &nbsp;<i class="feather icon-chevron-down"></i> -->Action</button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">


													<a style="color:teal; font-weight: bold;" class="dropdown-item" href="{{url('/barangaywelcome/edit')}}/{{\Crypt::encrypt($data->barangay_welcome_id)}}" class="btn btn-success"> <i class="icofont icofont-edit"></i>&nbsp; Edit</a>

													<a onclick="deleteConfirmation({{$data->barangay_welcome_id}})" style="color:red; font-weight: bold;" class="dropdown-item"><i class="icofont icofont-trash"></i>&nbsp;Delete</a>



												</div>
											</td>
										</tr>


										@endforeach
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>



			</div>
		</div>

	</div>
</div>










<form role="form" method="post" action="{{url('/barangaywelcome/submit')}}" enctype="multipart/form-data" class="form_submit" data-value="{{$barangay->barangay_id}}">
	{{csrf_field()}}
	<div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg card business-info services m-b-20" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<center>
						<h4 class="modal-title" id="myModalLabel">Add Announcement</h4>
					</center>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="icon-list-demo">
						</div>
					</div>
					<div class="col-md-12">

						<div class="form-group row">
							<div class="col-sm-10">
								<input type="hidden" name="barangay_id" value="{{$barangay->barangay_id}}" class="form-control">
								<input name="invisible" type="hidden" value="secret">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title</label>
							<div class="col-sm-10">
								<input type="text" name="title" class="form-control" placeholder="Enter Title" required="true">
							</div>
						</div>



						

						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Upload Image</label>
							<div class="col-sm-10">
								<input type="file" id="input_file" name="display_image" class="form-control">
							</div>
						</div>




						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description</label>
							<div class="col-sm-10">
								<textarea name="description" rows="12" cols="7" class="form-control"></textarea>
							</div>
						</div>

						
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">About Description</label>
							<div class="col-sm-10">
								<textarea name="about_description" rows="12" cols="7" class="form-control"></textarea>
							</div>
						</div>


					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="create" class="btn btn-success">Send</button>
				<button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
			</form>


		</div>
	</div>
</div>
</div>




<script type="text/javascript">


	$(".form_submit").submit(function(e) {
		$('#myModal').modal('hide');

		e.preventDefault();

		if ($('#input_file').get(0).files.length === 0) {
			swal({
				title: 'No image uploaded',
				html: 'Please upload image to submit.',
				type: 'info'
			});

			return false;
		}

		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){

					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);
          
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		var barangay_id = $('.form_submit').attr('data-value');
      		setTimeout(function(){
      			window.location.href = '{{ url("/barangay-welcome")}}/{{\Crypt::encrypt($barangay->barangay_id)}}';
      		}, 1500);
      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1500);
      	}

      },
      resetForm: true
  });
		}, 1500);
	});




	function deleteConfirmation(barangay_welcome_id) {
		swal({
			title:"Are you sure you want to delete this?",
			text: "You won't be able to revert this!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, delete it!",
		}).then(function (e) {

			if (e.value === true) {
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$.ajax({
					type: 'POST',
					url: "{{url('/deletebarangaywelcome')}}/" + barangay_welcome_id,
					data: {_token: CSRF_TOKEN},
					dataType: 'JSON',
					error: function (xhr, status, errorThrown) {
              // Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
          },
          success: function (results) {
          	if (results.success === true) {
          		swal({
          			title: "Done!",
          			text: results.message,
          			type: "success",
          			showConfirmButton: false
          		});
          		setTimeout(function(){
          			location.reload(true);
          		}, 1500);

          	} else {
          		swal({
          			title: "Error!",
          			text: results.message,
          			type: "error"
          		});
          		setTimeout(function(){
          			location.reload(true);
          		}, 1500);           
          	}
          }
      });

			} else {
				e.dismiss;
			}

		}, function (dismiss) {
			return false;
		})
	}
</script>
@endsection