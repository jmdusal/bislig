@extends('admin.header_admin')

@section('title')
Edit {{$data->title}}
@endsection

@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">

                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">

              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="card-header-right">
                  </div>
                </div>

                <div class="card-block">
                  <h4 class="sub-title">Edit Welcome Data</h4>

                  <form method="post" action="{{url('/barangaywelcome', $data->barangay_welcome_id)}}" enctype="multipart/form-data" class="form_submit">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="PATCH">



                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Title</label>
                      <div class="col-sm-10">
                        <input type="text" name="title" class="form-control" value="{{$data->title}}" required="True">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Upload Image File</label>
                      <div class="col-sm-10">
                        <input type="file" name="display_image" class="form-control">
                        <img src="{{ asset('storage/barangay_welcome_images')}}/{{$data->display_image}}" class="img-thumbnail" width="100" />
                        <input type="hidden" name="hidden_image" value="{{ $data->display_image}}" />
                      </div>
                    </div>


                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <textarea name="description" rows="12" cols="7" class="form-control">{{$data->description}}</textarea>
                      </div>
                    </div>


                    <br><br><br><br>

                    <div class="form-group row justify-content-end" >
                      <a href="{{ URL::previous() }}" class="btn btn-default mr-3">Back</a>
                      <button type="submit" class="btn btn-success mr-3"><i class="feather icon-save"></i>&nbsp;Save</button>
                    </div>

                    <!-- <div class="form-group">
                      <input type="submit" class="btn btn-primary" value="Edit">
                    </div> -->
                  </form>



                </div>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>

  </div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
  $(".form_submit").submit(function(e) {
    e.preventDefault();
    swal({
      html:   '<div class="loader-block">'+
      '<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
      '</div>'+
      '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>',
      allowOutsideClick: false,
      showConfirmButton:false,
    });
    setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
    setTimeout(function(){
      $('.form_submit').ajaxSubmit({
        beforeSubmit: function(){
          $('.progress-bar').width('0%')
        },
        uploadProgress: function(event, position, total, percentComplete){
          $('#progress-status').text(percentComplete+'%');
        },            
        error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
            title: "Error!",
            text: "You`ve insert wrong file type! Please upload an image file!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
        },
        success: function(results){
          console.log(results);
          if(results.success == true){
            $('.progress-bar').width('0%').html('');
            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });


            setTimeout(function(){

              window.location.href = '{{ url("/barangay-welcome")}}/{{\Crypt::encrypt($data->barangay_id)}}';
            }, 1500);


            
          }else{
            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            location.reload(true);
          }
        },
        resetForm: true
      });

    }, 1500);
  });
</script>

@endsection