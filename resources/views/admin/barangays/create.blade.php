@extends('admin.header_admin')

@section('title')
Add Barangay
@endsection

@section('content')

@section('stylesheets')
<script>
  tinymce.init({

    selector: 'textarea',
    height: 500,
    menubar: false,
    plugins: [

    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],

  });

  .container {
    position: relative;
    width: 35%;
  }

  .img-thumbnail {
    opacity: 1;
    display: block;
    width: 100%;
    height: auto;
    transition: .5s ease;
    backface-visibility: hidden;
  }

  .middle {
    transition: .5s ease;
    opacity: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
  }

  .container:hover .image {
    opacity: 0.3;
  }

  .container:hover .middle {
    opacity: 1;
  }

  .text {
    background-color: #FFFFFF;
    color: white;
    font-size: 20px;
    padding: 4px;
  }

  .ul{

    color: black;
    font-weight: bolder;
    font-size: 20px;
    padding: 1px;
  }
  
</script>
@endsection

<div class="pcoded-content">
  <div class="pcoded-inner-content">




    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">


                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">

              </div>
            </div>
          </div>
        </div>

        <div class="page-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="card">
                <div class="card-header">

                  <div class="card-header-right">

                  </div>
                </div>




                <div class="card-block">
                  <h4 class="sub-title">Add Barangay </h4>
                  <form role="form" method="post" action="{{url('/barangays/submit')}}" enctype="multipart/form-data" class="form_submit">
                    {{csrf_field()}}

                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Barangay Name</label>
                      <div class="col-sm-10">
                        <input type="text" name="barangay_name" class="form-control" required="True" placeholder="Enter Barangay Name">
                      </div>
                    </div>


                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Upload Image File</label>
                      <div class="col-sm-10">
                        <input type="file" id="input_file" name="display_image" class="form-control" required>

                      </div>
                    </div>


                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <textarea name="description" rows="12" cols="7" class="form-control"></textarea>
                      </div>
                    </div>


                    
                    <input type="hidden" name="user_id" value="{{Auth::user()->user_id}}" class="form-control">



                    <br><br><br><br>

                    <div class="form-group row justify-content-end" >
                      <a href="{{ URL::previous() }}" class="btn btn-default mr-3">Back</a>
                      <button type="create" class="btn btn-success mr-3"><i class="feather icon-save"></i>&nbsp;Submit</button>
                    </div>

                    


                  </form>
                </div>

                
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>


<script type="text/javascript">




  $(".form_submit").submit(function(e) {

    e.preventDefault();

    if ($('#input_file').get(0).files.length === 0) {
      swal({
        title: 'No image uploaded',
        html: 'Please upload image to submit.',
        type: 'info'
      });


      return false;
    }

    swal({
      html:   '<div class="loader-block">'+
      '<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
      '</div>'+
      '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
      '<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
      allowOutsideClick: false,
      showConfirmButton:false,
    });
    setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);

    setTimeout(function(){

      $('.form_submit').ajaxSubmit({
        beforeSubmit: function(){
          $('.progress-bar').width('0%')
        },
        uploadProgress: function(event, position, total, percentComplete){

          $('#progress-status').text(percentComplete+'%');
        },            
        error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
            title: "Error!",
            text: "You`ve insert wrong file type! Please upload an image file!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
        },
        success: function(results){
          console.log(results);
          if(results.success == true){
            $('.progress-bar').width('0%').html('');
            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });
            setTimeout(function(){
              window.location.href = '{{ url("/dashboard")}}';
            }, 1500);
          }else{

            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1500);

          }
        },
        resetForm: true
      });

    }, 1500);
  });

</script>

@endsection