@extends('admin.header_admin')

@section('title')
Barangay {{$barangay->barangay_name}} Gallery 
@endsection

@section('stylesheet')
<style type="text/css">
	.table td{
		max-width: 200px; 
		min-width: 70px;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;

	}
</style>
<script>
	tinymce.init({

		selector: 'textarea',
		height: 200,
		menubar: false,
		plugins: [

		'advlist autolink lists link image charmap print preview anchor',
		'searchreplace visualblocks code fullscreen',
		'insertdatetime media table paste code help wordcount'
		],

	});
</script>

@endsection
@section('content')

<div class="pcoded-content">
	<div class="pcoded-inner-content">

		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">
									
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Image</button>
								</div>
							</div>
						</div>
						
					</div>
				</div>


				














				<div class="page-body">
					<div class="card">
						<div class="card-header">

							
							<center>
								<h4>Barangay Gallery Table</h4>
							</center>
							
							
							

						</div>
						<div class="card-block">
							<div class="dt-responsive table-responsive">
								<table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
									<thead>
										<tr>
											<th width="20%">Image</th>
											


											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>

										@foreach($barangaygalleries as $data)
										<tr>
											<td>
												<a href="{{ asset('storage/barangay_gallery_images')}}/{{$data->display_image}}" data-lightbox="gallery">
													<img src="{{ asset('storage/barangay_gallery_images')}}/{{$data->display_image}}" class="img-thumbnail" width="200">
												</a>
											</td>
											
											<td class="text-center">
												

												<a onclick="deleteConfirmation({{$data->barangay_gallery_id}})" style="color:white; " class="btn btn-danger"><i class="icofont icofont-trash"></i>&nbsp;Remove</a>

											</td>


											
										</tr>


										@endforeach
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>



			</div>
		</div>

	</div>
</div>






















<form role="form" method="post" action="{{url('/barangaygallery/submit')}}" enctype="multipart/form-data" class="form_submit" data-value="{{$barangay->barangay_id}}">
	{{csrf_field()}}
	<div class="modal fade modal-icon" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg card business-info services m-b-20" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<center>
						<h4 class="modal-title" id="myModalLabel">Add Image</h4>
					</center>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">


					<div class="col-md-12 text-center">
						<div class="icon-list-demo">
						</div>
					</div>
					<div class="col-md-12">

						<div class="form-group row">
							<div class="col-sm-10">
								<input type="hidden" name="barangay_id" value="{{$barangay->barangay_id}}" class="form-control">
								<input name="invisible" type="hidden" value="secret">
							</div>
						</div>



						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Upload Image</label>
							<div class="col-sm-10">
								<input type="file" id="input_file" name="display_image" class="form-control">
							</div>
						</div>


						<input type="date" name="date_taken" class="form-control" value="<?php echo date('Y-m-d'); ?>" hidden>

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="create" class="btn btn-success mr-3"><i class="feather icon-save"></i>&nbsp;Submit</button>
				<button type="button" class="btn btn-proccess" data-dismiss="modal">Close</button>
			</div>
		</form>





	</div>
</div>
</div>
</div>




<script type="text/javascript">


	$(".form_submit").submit(function(e) {
		$('#myModal').modal('hide');

		e.preventDefault();


		if ($('#input_file').get(0).files.length === 0) {
			swal({
				title: 'No image uploaded',
				html: 'Please upload image to submit.',
				type: 'info'
			});


			return false;
		}

		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){

					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
          	title: "Error!",
          	text: "You`ve insert wrong file type! Please upload an image file!",
          	type: "error",
          	showConfirmButton: true,
          });
          // location.reload(true);
          
          console.log(xhr.responseText);
      },
      success: function(results){
      	console.log(results);
      	if(results.success == true){
      		$('.progress-bar').width('0%').html('');
      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		var barangay_id = $('.form_submit').attr('data-value');
      		setTimeout(function(){
      			window.location.href = '{{ url("/barangay-galleries")}}/{{\Crypt::encrypt($barangay->barangay_id)}}';
      		}, 1500);
      	}else{
      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1500);
      	}

      },
      resetForm: true
  });
		}, 1500);
	});




	function deleteConfirmation(barangay_gallery_id) {
		swal({
			title:"Are you sure you want to delete this?",
			text: "You won't be able to revert this!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: "Cancel",
			confirmButtonText: "Yes, delete it!",
		}).then(function (e) {

			if (e.value === true) {
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$.ajax({
					type: 'POST',
					url: "{{url('/deletebarangaygallery')}}/" + barangay_gallery_id,
					data: {_token: CSRF_TOKEN},
					dataType: 'JSON',
					error: function (xhr, status, errorThrown) {
              // Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
          },
          success: function (results) {
          	if (results.success === true) {
          		swal({
          			title: "Done!",
          			text: results.message,
          			type: "success",
          			showConfirmButton: false
          		});
          		setTimeout(function(){
          			location.reload(true);
          		}, 1500);

          	} else {
          		swal({
          			title: "Error!",
          			text: results.message,
          			type: "error"
          		});
          		setTimeout(function(){
          			location.reload(true);
          		}, 1500);           
          	}
          }
      });

			} else {
				e.dismiss;
			}

		}, function (dismiss) {
			return false;
		})
	}
</script>
@endsection