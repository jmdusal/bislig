@extends('admin.header_admin')

@section('title')
Barangay Table
@endsection

@section('stylesheet')
<style type="text/css">
  .table td{
    max-width: 200px; 
    min-width: 70px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

  }
</style>
<script>
  tinymce.init({

    selector: 'textarea',
    height: 200,
    menubar: false,
    plugins: [

    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
    ],

  });
</script>

@endsection
@section('content')
<div class="pcoded-content">
  <div class="pcoded-inner-content">

    <div class="main-body">
      <div class="page-wrapper">

        <div class="page-header">
          <div class="row align-items-end">
            <div class="col-lg-8">
              <div class="page-header-title">
                <div class="d-inline">
                  <!-- <a href="{{url('/barangay/add')}}" class="btn btn-success">Add Barangay</a> -->
                  <!-- <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Add Official</button> -->
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="page-header-breadcrumb">

              </div>
            </div>
          </div>
        </div>



        
        <div class="page-body">
          <div class="card">
            <div class="card-header">
              <h4>Barangay</h4>

            </div>
            <div class="card-block">
              <div class="dt-responsive table-responsive">
                <table id="table-style-hover" class="table table-striped table-hover table-bordered nowrap">
                  <thead>
                    <tr>
                      <th width="1%">Image</th>
                      <th>Barangay Name</th> 
                      <th>Description</th>
                      
                      
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach($barangays as $row)
                    <tr>
                      <td>
                        <a href="{{ asset('storage/barangays_images')}}/{{$row->display_image}}" data-lightbox="gallery">
                          <img src="{{ asset('storage/barangays_images')}}/{{$row->display_image}}" class="img-thumbnail" width="200">
                        </a>
                      </td>

                      <td>{{$row->barangay_name}}</td>
                      <td>{!!$row->description!!}</td>

                      
                      <td class="text-center">

                        <button class="btn btn-success" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!-- &nbsp;<i class="feather icon-chevron-down"></i> -->Action</button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                          <a style="color:green; font-weight: bold;" class="dropdown-item" href="{{url('/barangay/data')}}/{{\Crypt::encrypt($row->barangay_id)}}" class="btn btn-primary"> <i class="icofont icofont-eye-alt"></i>&nbsp; View</a>


                          <a style="color:teal; font-weight: bold;" class="dropdown-item" href="{{url('/barangay/edit')}}/{{\Crypt::encrypt($row->barangay_id)}}" class="btn btn-primary"> <i class="icofont icofont-edit"></i>&nbsp; Edit</a>


                          <!-- <a onclick="deleteConfirmation({{$row->barangay_id}})" style="color:red; font-weight: bold;" class="dropdown-item"> <i class="icofont icofont-trash"></i>&nbsp; Delete</a> -->
                          


                        </div>
                        
                      </td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    
  </div>
</div>
</div>
</div>
</div>
</div>







<script type="text/javascript">



  $(".form_submit").submit(function(e) {
    $('#myModal').modal('hide');
    e.preventDefault();
    swal({
      html:   '<div class="loader-block">'+
      '<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
      '</div>'+
      '<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>',    
      allowOutsideClick: false,
      showConfirmButton:false,
    });
    setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
    setTimeout(function(){
      $('.form_submit').ajaxSubmit({
        beforeSubmit: function(){
          $('.progress-bar').width('0%')
        },
        uploadProgress: function(event, position, total, percentComplete){
          $('#progress-status').text(percentComplete+'%');
        },
        error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;

          swal({
            title: "Error!",
            text: "Please check input fields!",
            type: "error",
            showConfirmButton: true,
          });
          // location.reload(true);

          console.log(xhr.responseText);
        },
        success: function(results){
          console.log(results);
          if(results.success == true){
            $('.progress-bar').width('0%').html('');
            swal({
              title: "Done!",
              text: results.message,
              type: "success",
              showConfirmButton: false
            });
            setTimeout(function(){
              window.location.href = '{{ url("/barangay/table")}}';
            }, 1500);
            
          }else{
            swal({
              title: "Error!",
              text: results.message,
              type: "error"
            });
            setTimeout(function(){
              location.reload(true);
            }, 1500);
          }
        },
        resetForm: true
      });

    }, 1500);
  });





  function deleteConfirmation(barangay_id) {
    swal({
      title:"Are you sure you want to delete this?",
      text: "You won't be able to revert this!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      cancelButtonText: "No, cancel!",
      confirmButtonText: "Yes, delete it!",
    }).then(function (e) {

      if (e.value === true) {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
          type: 'POST',
          url: "{{url('/deletebarangay')}}/" + barangay_id,
          data: {_token: CSRF_TOKEN},
          dataType: 'JSON',
          error: function (xhr, status, errorThrown) {
              //Here the status code can be retrieved like;
              xhr.status;
              console.log(xhr.responseText);
            },
            success: function (results) {
              if (results.success === true) {
                swal({
                  title: "Done!",
                  text: results.message,
                  type: "success",
                  showConfirmButton: false,
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1500);
              } else {
                swal({
                  title: "Error!",
                  text: results.message,
                  type: "error"
                });
                setTimeout(function(){
                  location.reload(true);
                }, 1500);
              }
            }
          });

      } else {
        e.dismiss;
      }

    }, function (dismiss) {
      return false;
    })
  }
</script>




@endsection