@extends('admin.header_admin')

@section('title')
Change Password
@endsection


@section('content')


<div class="pcoded-content">
	<div class="pcoded-inner-content">


		<div class="main-body">
			<div class="page-wrapper">

				<div class="page-header">
					<div class="row align-items-end">
						<div class="col-lg-8">
							<div class="page-header-title">
								<div class="d-inline">

								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="page-header-breadcrumb">

							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header">
									<div class="card-header-right">
									</div>
								</div>

								<div class="card-block">
									<h4 class="sub-title">Change Password</h4>

									<form method="post" action="{{ route('changePassword') }}" class="form_submit">
										{{csrf_field()}}

										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Current Password</label>
											<div class="col-sm-5">
												<input id="current-password" type="password" class="form-control" name="current-password" required>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2 col-form-label">New Password</label>
											<div class="col-sm-5">
												<input id="new-password" type="password" class="form-control" name="new-password" required>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Confirm New Password</label>
											<div class="col-sm-5">
												<input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
											</div>
										</div>


										<br>
										<input type="checkbox" onclick="myFunctionshowpassword()">&nbsp;Show Password
										<br><br><br>

										<button type="submit" class="btn btn-success">
											Change Password
										</button>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>





<script type="text/javascript">
	$(".form_submit").submit(function(e) {
		e.preventDefault();
		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/rload.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>'+
			'<small><span id="TakeWhile" hidden> This may take a while, depends on file size and internet speed</span></small>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},            
				error: function (xhr, status, errorThrown) {
          //Here the status code can be retrieved like;
          xhr.status;
          swal({
          	title: "Error!",
          	text: "Please check some input fields!",
          	type: "error",
          	showConfirmButton: true
          });
          // location.reload(true);

          console.log(xhr.responseText);
      },
      success: function(results){

      	console.log(results);

      	if(results.success == true){

      		$('.progress-bar').width('0%').html('');

      		swal({
      			title: "Done!",
      			text: results.message,
      			type: "success",
      			showConfirmButton: false
      		});
      		setTimeout(function(){
      			window.location.href = '{{ url("/dashboard")}}';
      		}, 1000);
      	}else{

      		swal({
      			title: "Error!",
      			text: results.message,
      			type: "error"
      		});
      		setTimeout(function(){
      			location.reload(true);
      		}, 1000);
      	}

      },
      resetForm: true
  });

		}, 1500);
	});


	function myFunctionshowpassword() {
		var x = document.getElementById("current-password");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}

		var y = document.getElementById("new-password");
		if (y.type === "password") {
			y.type = "text";
		} else {
			y.type = "password";
		}

		var z = document.getElementById("new-password-confirm");
		if (z.type === "password") {
			z.type = "text";
		} else {
			z.type = "password";
		}


	}

</script>








@endsection