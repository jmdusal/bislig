<!DOCTYPE html>
<html lang="en">
<head>
	<title>@yield('title') </title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
	<meta name="author" content="#">

	
	<!-- <link rel="icon" href="{{ asset('adminty/files/assets/images/favicon.ico')}}" type="image/x-icon"> -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

	<!-- HOME DASHBOARD LINKS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/feather/css/feather.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/css/jquery.mCustomScrollbar.css')}}">


	<!-- GALLERIES UPLOAD LINKS -->
	<link href="{{ asset('adminty/files/assets/pages/jquery.filer/css/jquery.filer.css')}}" type="text/css" rel="stylesheet" />
	<link href="{{ asset('adminty/files/assets/pages/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css')}}" type="text/css" rel="stylesheet" />


	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/themify-icons/themify-icons.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/icofont/css/icofont.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/icon/feather/css/feather.css')}}">

	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">

	<!-- new add scripted -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css')}}" />


	<link rel="stylesheet" type="text/css" href="{{asset('css/ekko-lightbox.css')}}">

	<link rel="stylesheet" type="text/css" href="{{asset('css/sweetalert2.min.css')}}">
	<link href="{{ asset('design/website/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />


	<!-- CALENDAR -->
	<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

	<!-- <script src="{{asset('js/moment.min.js')}}"></script> -->
	<!-- <script src="{{asset('js/jquery.min.js')}}"></script> -->
	<!-- <script src="{{asset('js/fullcalendar.min.js')}}"></script> -->
	<!-- <script src="{{asset('js/fullcalendar.min.css')}}"></script> -->

	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/fullcalendar/css/fullcalendar.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/fullcalendar/css/fullcalendar.print.css')}}" media='print'>



	<link rel="icon" href="{{ asset('bislig_logo.png')}}" type="image/gif" sizes="16x16">

	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/bower_components/jquery.steps/css/jquery.steps.css')}}">



	<link rel="stylesheet" type="text/css" href="{{ asset('adminty/files/assets/pages/social-timeline/timeline.css')}}">
	
	@yield('stylesheet')


	<style type="text/css">
		::-webkit-scrollbar {
			-webkit-appearance: none;
			width: 10px;
		}

		::-webkit-scrollbar-thumb {
			border-radius: 5px;
			background-color: rgba(0,0,0,.5);
			-webkit-box-shadow: 0 0 1px rgba(255,255,255,.5);
		}
	</style>
</head>
<body>
	<div class="theme-loader">
		<div class="ball-scale">
			<div class='contain'>


				<!-- <div class="cell preloader5 loader-block">
					<div class="circle-5 l"></div>
					
					<div class="circle-5 m"></div>

					<div class="circle-5 r"></div>
				</div> -->




				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>
				<div class="ring">
					<div class="frame"></div>
				</div>

				





			</div>
		</div>
	</div>

	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<nav class="navbar header-navbar pcoded-header">
				<div class="navbar-wrapper">
					<div class="navbar-logo">
						<a class="mobile-menu" id="mobile-collapse" href="#!">
							<i class="feather icon-menu"></i>
						</a>
						<a href="{{url('/')}}">
							
							<div class="row" style="padding-left: 20px; padding-right: 20px">

								

								<h4><bold>BISLIG</bold></h4>
								<!-- <img class="img-fluid" style="height: 48px; width: 75%;" src="{{ asset('bislig_logo.png')}}" alt="Theme-Logo" /> -->

								
							</div>
							
						</a>
						<a class="mobile-options">
							<i class="feather icon-more-horizontal"></i>
						</a>
					</div>
					<div class="navbar-container container-fluid">
						<ul class="nav-left">
							<li class="header-search">
								<div class="main-search morphsearch-search">
									<div class="input-group">
										<!-- <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
										<input type="text" class="form-control">
										<span class="input-group-addon search-btn"><i class="feather icon-search"></i></span> -->
									</div>
								</div>
							</li>
						<!--<li>
								<a href="#!" onclick="javascript:toggleFullScreen()">
									<i class="feather icon-maximize full-screen"></i>
								</a>
							</li> -->
						</ul>
						<ul class="nav-right">
							<li class="header-notification">
								<div class="dropdown-primary dropdown">
									<div class="dropdown-toggle" data-toggle="dropdown">
										<!-- <i class="feather icon-bell"></i>
											<span class="badge bg-c-pink">5</span> -->
										</div>
										<ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
											<li>
												<h6>Notifications</h6>
												<label class="label label-danger">New</label>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">John Doe</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">Joseph William</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
											<li>
												<div class="media">
													<img class="d-flex align-self-center img-radius" src="{{ asset('adminty/files/assets/images/avatar-4.jpg')}}" alt="Generic placeholder image">
													<div class="media-body">
														<h5 class="notification-user">Sara Soudein</h5>
														<p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
														<span class="notification-time">30 minutes ago</span>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</li>

								<li class="user-profile header-notification">
									<div class="dropdown-primary dropdown">
										<div class="dropdown-toggle" data-toggle="dropdown">


											<!-- {{-- <img src="{{ asset('adminty/files/assets/images/iconavatar.png')}}" class="img-radius" alt="User-Profile-Image"> --}} -->

											<img src="{{ asset('admin_avatar.png')}}" class="img-radius" alt="User-Profile-Image">




											<span>{{Auth::user()->name}}</span>

											<i class="feather icon-chevron-down"></i>
											



											

										</div>


										<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">


											

											


											

											<li>
												<a href="{{ url('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
													<i class="feather icon-log-out"></i> Logout
												</a>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
													{{ csrf_field() }}
												</form>
											</li>

										</ul>


									</div>
								</li>








							</ul>
						</div>
					</div>
				</nav>



				<div class="pcoded-main-container">
					<div class="pcoded-wrapper">
						<nav class="pcoded-navbar">
							<div class="pcoded-inner-navbar main-menu">
								<li class="pcoded-hasmenu">

									<div class="pcoded-navigatio-lavel"><b>Home</b></div>

									<ul class="pcoded-item pcoded-left-item">

										<li class=" <?php echo $sidebar == "dashboard" ? 'active' : ''?>">
											<a href="{{url('/dashboard')}}">
												<span class="pcoded-micon st6"><i class="feather icon-home"></i></span>
												<!-- <span class="micon st6"><i class="feather icon-command"></i></span> -->
												<span class="pcoded-mtext">Dashboard</span>
												<span class="pcoded-badge label label-info active">Admin</span>
												
												<!-- <span class="pcoded-badge label label-success">Admin</span> -->
											</a>
										</li>

									</ul>

								</nav>


								<div class="main-content">
									@yield('content')
								</div>










								<script data-cfasync="false" src="{{ asset('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script><script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery/js/jquery.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/popper.js/js/popper.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/modernizr/js/modernizr.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/chart.js/js/Chart.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/widget/amchart/amcharts.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/widget/amchart/serial.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/widget/amchart/light.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/assets/js/SmoothScroll.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/js/pcoded.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/js/vartical-layout.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/dashboard/custom-dashboard.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/assets/js/script.min.js')}}"></script>
								<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>



								<script src="{{ asset('adminty/files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/data-table/js/jszip.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
								<script src="{{ asset('adminty/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/data-table/js/data-table-custom.js')}}"></script>



								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next/js/i18next.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
								<script type="text/javascript" src="{{ asset('adminty/files/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>




								<script type="text/javascript" src="{{ asset('adminty/files/assets/js/script.js')}}"></script>




								<script src="{{ asset('adminty/files/assets/pages/wysiwyg-editor/js/tinymce.min.js')}}"></script>
								<script src="{{ asset('adminty/files/assets/pages/wysiwyg-editor/wysiwyg-editor.js')}}"></script>



								<!-- <script src="{{ asset('alert/sweetalert2.all.min.js')}}"></script> -->

								<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script> -->

								<!-- <script src="{{URL::asset('assets/js/sweetalert2.all.min.js')}}"></script> -->


<!-- 
<script src="{{ asset('alert/sweetalert2.all.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->




<!-- <script src="{{ asset('javascripts/base_url.js')}}"></script> -->


<script src="{{ asset('adminty/files/assets/pages/jquery.filer/js/jquery.filer.min.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/filer/custom-filer.js')}}" type="text/javascript"></script>
<script src="{{ asset('adminty/files/assets/pages/filer/jquery.fileuploads.init.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/icon-modal.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/ckeditor/ckeditor.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/chart/echarts/js/echarts-all.js')}}" type="text/javascript"></script>


<script src="{{asset('js/ekko-lightbox.min.js')}}"></script>

<script src="{{asset('js/sweetalert2.all.min.js')}}"></script>

<script src="{{ asset('design/website/js/lightbox.min.js')}}"></script>
<script src="{{asset('js/jquery.form.min.js')}}"></script>


<script src="{{ asset('adminty/files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{ asset('adminty/files/assets/pages/widget/amchart/light.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/widget/custom-widget1.js')}}"></script>




<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/full-calender/calendar.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/js/classie.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/moment/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('adminty/files/bower_components/fullcalendar/js/fullcalendar.min.js')}}"></script>






<script src="{{ asset('adminty/files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{ asset('adminty/files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/form-validation/validate.js')}}"></script>

<script src="{{ asset('adminty/files/assets/pages/forms-wizard-validation/form-wizard.js')}}"></script>



<script src="{{asset('js/base_url.js')}}"></script>



<script type="text/javascript" src="{{ asset('adminty/files/assets/pages/social-timeline/social.js')}}"></script>















<script type="text/javascript">
	$(".form_submit_changepassword").submit(function(e) {
		$('#modal2').modal('hide');
		e.preventDefault();

		swal({
			html:   '<div class="loader-block">'+
			'<img src="'+'{{ asset("adminty/loading.gif")}}'+'" width="150" height="150">'+
			'</div>'+
			'<h4 class="text-center">Please Wait... <span id="progress-status"></span></h4>',    
			allowOutsideClick: false,
			showConfirmButton:false,
		});

		setTimeout(function(){ $('#TakeWhile').prop('hidden', false) }, 2500);
		setTimeout(function(){

			$('.form_submit_changepassword').ajaxSubmit({
				beforeSubmit: function(){
					$('.progress-bar').width('0%')
				},
				uploadProgress: function(event, position, total, percentComplete){
					$('#progress-status').text(percentComplete+'%');
				},
				error: function (xhr, status, errorThrown) {
            //Here the status code can be retrieved like;
            xhr.status;
            swal({
            	title: "Error!",
            	text: "Please check your old-password and confirm-password!",
            	type: "error",
            	showConfirmButton: true
            });
            // location.reload(true);
            console.log(xhr.responseText);
        },
        success: function(results){
        	console.log(results);

        	if(results.success == true){
        		$('.progress-bar').width('0%').html('');
        		swal({
        			title: "Done!",
        			text: results.message,
        			type: "success",
        			showConfirmButton: false
        		});
        		setTimeout(function(){
        			window.location.href = '{{ url("/dashboard")}}';
        		}, 1500);
        	}else{
        		swal({
        			title: "Error!",
        			text: results.message,
        			type: "error"
        		});
        		setTimeout(function(){
        			location.reload(true);
        		}, 1500);
        	}

        },
        resetForm: true
    });

		}, 1500);
	});



	// function myFunctionshowpassword() {
	// 	var x = document.getElementById("current-password");
	// 	if (x.type === "password") {
	// 		x.type = "text";
	// 	} else {
	// 		x.type = "password";
	// 	}

	// 	var y = document.getElementById("new-password");
	// 	if (y.type === "password") {
	// 		y.type = "text";
	// 	} else {
	// 		y.type = "password";
	// 	}

	// 	var z = document.getElementById("new-password-confirm");
	// 	if (z.type === "password") {
	// 		z.type = "text";
	// 	} else {
	// 		z.type = "password";
	// 	}


	// }









</script>

<script>

	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-23581568-13');
</script>




</body>
</html>
