@extends('welcome')


@section('title')
Bislig | Barangay {{$barangay[0]->barangay_name}}
@endsection

@section('content')
<div class="main-container">
	<section class="cover height-70 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
		<ul class="slides">
			<li class="imagebg" data-overlay="4">
				<div class="background-image-holder background--top">
					<img alt="background" src="{{ asset('storage/barangay_welcome_images')}}/{{$barangay_welcome[0]->display_image}}" />
				</div>
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-md-12">
							<h1>

                {{$barangay_welcome[0]->title}}
                
              </h1>
              <p>{!!$barangay_welcome[0]->description!!}</p>
            </div>
          </div>
        </div>
      </li>

    </ul>
  </section>








  @foreach($latest_announcement as $row)
  <section class="feature-large feature-large-2 bg--secondary">
   <div class="container">
    <div class="row justify-content-around">
     <div class="col-md-4 col-lg-3">
      <h3>Update</h3>
      <p class="lead">
       {{$row->title}}
     </p>
   </div>
   <div class="col-md-4 col-lg-4">
    @if($row->display_image != 'noimage.jpg')
    <img style="object-fit: cover; width: 100%; height: 500px;" alt="Image" class="border--round box-shadow-wide" src="{{ asset('storage/announcement_images')}}/{{$row->display_image}}" />


    @else
    <img style="object-fit: cover; width: 100%; height: 500px;" alt="Image" class="border--round box-shadow-wide" src="{{ asset('noimage.jpg')}}" />
    @endif


  </div>
  <div class="col-md-4 col-lg-2">
    <hr class="short" />
    <p>
      {!!$row->description!!}
    </p>
    <a href="{{url('/announcement-details', $row->barangay_name)}}/{{\Crypt::encrypt($row->announcement_id)}}">Read more</a>
  </div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</section>
@endforeach





















@if(count($announcement) != 0)
<section class="bg--secondary">
  <div class="container">
    <div class="row">
     <div class="col-md-12">
      <div class="row">
       <div class="col-6">
        <h4>Announcements</h4>
      </div>
      <div class="col-6 text-right">
        <a href="{{url('/barangay/announcement', $barangay[0]->barangay_name)}}">View more</a>
      </div>
    </div>
  </div>
</div>
<div class="slider" data-paging="true">
 <ul class="slides">

  @foreach($announcement as $row)

  <li class="col-md-4 col-12">
   <div class="feature feature-1">


    @if($row->display_image != 'noimage.jpg')
    <a href="{{ asset('storage/announcement_images')}}/{{$row->display_image}}" class="border-round block" data-lightbox="gallery">
     <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/announcement_images')}}/{{$row->display_image}}" />
   </a>
   @else

   <a href="{{ asset('noimage.jpg')}}" class="border-round block" data-lightbox="gallery">
     <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('noimage.jpg')}}" />
   </a>

   @endif


   <div class="feature__body boxed boxed--border">
     <h5>{{$row->title}}</h5>
     <p>
      ADMINISTRATOR'S CORNER
    </p>

    <a href="{{url('/announcement-details', $row->barangay_name)}}/{{\Crypt::encrypt($row->announcement_id)}}">
      Read more
    </a>
  </div>
</div>
</li>

@endforeach

</ul>
</div>
</div>
</section>
@else

@endif
























@if(count($barangaygallery) != 0)

<section class="bg--secondary">
 <div class="container">
  <div class="row">
   <div class="col-md-12">
    <div class="row">
     <div class="col-6">
      <h4>Gallery</h4>
    </div>
    <div class="col-6 text-right">
      <a href="{{ url('barangay-gallery', $barangay[0]->barangay_name)}}">View more</a>
    </div>
  </div>
</div>
</div>


<div class="slider" data-paging="true">
 <ul class="slides">

  @foreach($barangaygallery as $row)


  <li class="col-md-4 col-12">
   <div class="feature feature-1">

    <a href="{{ asset('storage/barangay_gallery_images')}}/{{$row->display_image}}" class="border-round block" data-lightbox="gallery">
     <img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/barangay_gallery_images')}}/{{$row->display_image}}" />
   </a>


   <div class="feature__body boxed boxed--border">
     <!-- <h5>Luxury Apartment &mdash; Tribeca</h5> -->
     <p>
      <?php
      $orig_date =  explode('-', $row->date_taken);
      $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
      echo date("M d, Y", strtotime($con_date));
      ?>

    </p>
    <!-- <a href="#">View Image</a> -->
  </div>
</div>
</li>

@endforeach

</ul>
</div>
</div>
</section>

@else

@endif



























<section class="text-center imagebg" data-overlay='4'>
 <div class="background-image-holder">
  <img alt="background" src="{{ asset('frontendfiles/img/tinuy-an.jpg')}}" />
</div>
<div class="container">
  <div class="row">
   <div class="col-md-12 col-lg-10">
    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
    <div class="video-cover border--round">
     <div class="background-image-holder">
     </div>
     <div class="video-play-icon"></div>
     <iframe data-src="https://www.youtube.com/embed/Z6EaCgDydOw" allowfullscreen="allowfullscreen"></iframe>
   </div>
   <!--end video cover-->
   <a class="btn btn--primary type--uppercase" href="#">
     <span class="btn__text">
      View More Videos
    </span>
  </a>
</div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</section>


</div>



@endsection