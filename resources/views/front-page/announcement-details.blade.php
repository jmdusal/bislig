@extends('welcome')

@section('title')

Announcement | Detail
@endsection

@section('content')


<div class="main-container">

	
	<section>
		<div class="container">
			<div class="row">

				<div class="col-md-12 col-lg-12" align="center">
					<article class="masonry__item" data-masonry-filter="Web Design">
						
						<div class="article__body">

							
							<div class="slider mb-3 gallery" data-arrows="true">
								<ul class="slides">

									<li>
										@if($announcement->display_image != 'noimage.jpg')
										<a href="{{ asset('storage/announcement_images')}}/{{$announcement->display_image}}" data-lightbox="display_image">
											<img style="height: 400px; " class="gallery_image" alt="Image" src="{{ asset('storage/announcement_images')}}/{{$announcement->display_image}}" />
										</a>

										@else
										<a href="{{ asset('noimage.jpg')}}" data-lightbox="display_image">
											<img style="height: 400px; " class="gallery_image" alt="Image" src="{{ asset('noimage.jpg')}}" />
										</a>

										@endif
									</li>

								</ul>
							</div>



							<div class="article__title text-center">
								<h2>{{$announcement->title}}</h2>

								<span>
									Posted on <?php
									$orig_date =  explode('-', $announcement->date_created);
									$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
									echo date("M d, Y", strtotime($con_date));
									?>
								</span>
							</div>

							<p id="viewer-5fcgs" class="XzvDs _208Ie tFDi5 blog-post-text-font blog-post-text-color _2QAo- _25MYV _6RI6N tFDi5 public-DraftStyleDefault-block-depth0 public-DraftStyleDefault-ltr">{!!$announcement->description!!}</p>


							

						</div>
					</article>
				</div>




			</div>
		</div>
	</section>

	
</div>

@endsection