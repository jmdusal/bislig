@extends('welcome')

@section('title')

Barangay {{$barangay[0]->barangay_name}} | About
@endsection

@section('content')

<div class="main-container">
	<section class="cover height-30 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
		<ul class="slides">
			<li class="imagebg" data-overlay="4">
				<div class="background-image-holder background--top">
					<img alt="background" src="{{ asset('frontendfiles/img/tinuy-an.jpg')}}" />
				</div>
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-md-12">
							<h1>
								{{$barangay[0]->barangay_name}}
							</h1>
							<p>{!!$barangay[0]->description!!}</p>
						</div>
					</div>
				</div>
			</li>

        </ul>
    </section>

    
    <section class="text-center">
     <div class="container">

      @foreach($barangay_welcome as $row)
      <div class="row justify-content-center">
       <div class="col-md-10 col-lg-8">
        <h2>{{$row->title}}</h2>
        <p class="lead">
            {!!$row->about_description!!}
        </p>
    </div>
</div>

@endforeach


</div>

</section>

</div>

@endsection