@extends('welcome')

@section('title')

Bislig | Video
@endsection

@section('content')

<div class="main-container">
  <section class="cover height-30 imagebg text-center slider" data-arrows="true" data-paging="true">
    <ul class="slides">
      <li class="imagebg" data-overlay="4">
        <div class="background-image-holder background--top">
          <img alt="background" src="{{ asset('frontendfiles/img/tinuy-an.jpg')}}" />
        </div>
        <div class="container pos-vertical-center">
          <div class="row">
            <div class="col-md-12">

              <h1>
                Barangay {{$barangay[0]->barangay_name}} Videos
              </h1>



            </div>
          </div>
        </div>
      </li>
    </ul>
  </section>





  <section class=" ">
    <div class="container">
      <div class="masonry">
        <!-- <form method="POST" action="https://kauswagan.net/videos/search" class="form--horizontal row m-0">
          <input type="hidden" name="_token" value="tvgEE7RcJafWXCBetANerM8ArrKXaL6chFlIVxwY">
          <div class="col-md-8">
            <input type="text" name="q" placeholder="Type search keywords here" required="true"/>
          </div>
          <div class="col-md-4">
            <button type="submit" class="btn btn--primary type--uppercase">Search</button>
          </div>
        </form> -->
        <hr />

        @if(count($barangayvideo) != 0)
        
        <div class="masonry__container row">


          @foreach($barangayvideo as $key => $row)

          <div class="masonry__item col-md-6 col-12" data-masonry-filter="Television">
            <div class="video-cover border--round">

              <!-- <iframe width="560" height="315" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen> </iframe> -->


              {!!$row->embed_video!!}
            </div>
            <!-- <span class="h4 inline-block">Bislig Tourist</span> -->
          </div>

          @endforeach
        </div>

        @else
        
        <div class="col-md-12" align="center">
          <h3 style="color: red">--No data, Please upload video in-- <a href="{{url('/dashboard')}}" style="color: black; font-weight: inherit;">"Dashboard"</a></h3>
        </div>
        
        @endif




        <div class="pagination">
          <ol>
            {!! $barangayvideo->render() !!}
          </ol>
        </div>

      </div>
      
    </div>
    
  </section>









</div>

@endsection