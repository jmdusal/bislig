@extends('welcome')


@section('title')
Bislig | Barangay Officials
@endsection


@section('content')
<div class="main-container">

  <section class="cover height-30 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
    <ul class="slides">
     <li class="imagebg" data-overlay="4">
      <div class="background-image-holder background--top">
       <img alt="background" src="{{ asset('frontendfiles/img/tinuy-an.jpg')}}" />
     </div>
     <div class="container pos-vertical-center">
       <div class="row">
        <div class="col-md-12">
         <h1>

          Barangay Officials of Brgy.{{$barangay[0]->barangay_name}}

        </h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>
    </div>
  </div>
</li>

</ul>
</section>






@if(count($barangay_officials) != 0)
<section class="switchable feature-large">
 <div class="container">

  @foreach($barangay_officials as $row)
  <div class="row justify-content-around">

   @if($row->position == "Barangay Captain")

   <div class="col-md-6">
    <img style=" object-fit: cover; height: 420px; width: 100%;" alt="Image" class="border--round" src="{{ asset('storage/barangay_officials_images')}}/{{$row->display_image}}" />

    <!-- {{ asset('frontendfiles/img/gallery/avatar-wide-2.jpg')}} -->
  </div>
  <div class="col-md-6 col-lg-5">
    <div class="switchable__text">

     <div class="text-block">
      <h2>{{$row->barangay_official_name}}</h2>
      <span>{{$row->position}}</span>
      <br>
      <span>Date Elected:</span>
      <span>
       <?php
       $orig_date =  explode('-', $row->date_elected);
       $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
       echo date("M d, Y", strtotime($con_date));
       ?>
     </span>
   </div>
   <p class="lead">
    {!!$row->description!!}
  </p>

</div>
</div>
@endif

</div>
@endforeach
</div>
</section>
@else

@endif











<section class="switchable switchable--switch feature-large">
 <div class="container">
  <div class="row justify-content-around">
   <div class="col-md-6">
    <img alt="Image" class="border--round" src="{{ asset('frontendfiles/img/gallery/avatar-wide-2.jpg')}}" />
  </div>
  <div class="col-md-6 col-lg-5">
    <div class="switchable__text">
     <div class="text-block">
      <h2>Lucille Vogel</h2>
      <span>Vice Captain</span>
    </div>
    <p class="lead">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    </p>
  </div>
</div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</section>








@if(count($barangay_officials) != 0)
<section class="switchable feature-large">
 <div class="container">

  @foreach($barangay_officials as $row)

  <div class="row justify-content-around">

   @if($row->position == "Barangay Secretary")
   <div class="col-md-6">
    <img alt="Image" style="object-fit: cover; height: 420px; width: 100%;" class="border--round" src="{{ asset('storage/barangay_officials_images')}}/{{$row->display_image}}" />
  </div>
  <div class="col-md-6 col-lg-5">
    <div class="switchable__text">
     <div class="text-block">
      <h2>{{$row->barangay_official_name}}</h2>
      <span>{{$row->position}}</span>
      <br>
      <span>Date Elected:</span>
      <span>
       <?php
       $orig_date =  explode('-', $row->date_elected);
       $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
       echo date("M d, Y", strtotime($con_date));
       ?>
     </span>
   </div>
   <p class="lead">
    {!!$row->description!!}
  </p>
</div>
</div>

@endif

</div>

@endforeach

</div>
</section>
@else

@endif







@if(count($barangay_officials) != 0)
<section class="text-center">
 <div class="container">
  <div class="row justify-content-center">
   <div class="col-md-10 col-lg-8">
    <h2>Barangay Councilors</h2>
    <p class="lead">
     Lorem ipsum dolor sit amet, consectetur adipiscing elit.
   </p>
 </div>
</div>
<!--end of row-->
</div>
<!--end of container-->
</section>



<section class="text-center">
 <div class="container">
  <div class="row">

   @foreach($barangay_officials as $row)

   @if($row->position == "Barangay Councilor")

   <div class="col-md-4">
    <div class="feature feature-8">
      <img alt="Image" style="object-fit: cover; height: 200px; width: 50%;" src="{{ asset('storage/barangay_officials_images')}}/{{$row->display_image}}" />
      <h5>{{$row->barangay_official_name}}</h5>
      <span>{{$row->position}}</span>

      <br>
      <span>Date Elected:</span>
      <span>
        <?php
        $orig_date =  explode('-', $row->date_elected);
        $con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
        echo date("M d, Y", strtotime($con_date));
        ?>
      </span>
    </div>
  </div>


            			<!-- <div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-3.png')}}" />
            					<h5>Zach Smith</h5>
            					<span>2 Councilor</span>
            				</div>
            			</div>
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-2.png')}}" />
            					<h5>Bernice Lucas</h5>
            					<span>3 Councilor</span>
            				</div>
            			</div>
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-4.png')}}" />
            					<h5>Cameron Nguyen</h5>
            					<span>4 Councilor</span>
            				</div>
            			</div>
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-5.png')}}" />
            					<h5>Josie Web</h5>
            					<span>5 Councilor</span>
            				</div>
            			</div>
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-6.png')}}" />
            					<h5>Bryce Vaughn</h5>
            					<span>6 Councilor</span>
            				</div>
            			</div>
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" src="{{ asset('frontendfiles/img/gallery/avatar-round-1.png')}}" />
            					<h5>Bryce Vaughn</h5>
            					<span>7 Councilor</span>
            				</div>
            			</div> -->

            			@endif
            			@endforeach

            		</div>
            	</div>
            </section>
            @else

            @endif








            @if(count($barangay_officials) != 0)
            <section class="text-center">
            	<div class="container">
            		<div class="row justify-content-center">
            			<div class="col-md-10 col-lg-8">
            				<h2>SK Member</h2>
            				<p class="lead">
            					Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            				</p>
            			</div>
            		</div>
            		<!--end of row-->
            	</div>
            	<!--end of container-->
            </section>



            <section class="switchable feature-large">
            	<div class="container">

            		@foreach($barangay_officials as $row)

            		<div class="row justify-content-around">
            			@if($row->position == "SK Chairman")

            			<div class="col-md-6">
            				<img alt="Image" style="object-fit: cover; height: 400px; width: 100%;" class="border--round" src="{{ asset('storage/barangay_officials_images')}}/{{$row->display_image}}" />
            			</div>
            			<div class="col-md-6 col-lg-5">
            				<div class="switchable__text">
            					<div class="text-block">
            						<h2>{{$row->barangay_official_name}}</h2>
            						<span>{{$row->position}}</span>
            						<br>
            						<span>Date Elected:</span>
            						<span>
            							<?php
            							$orig_date =  explode('-', $row->date_elected);
            							$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
            							echo date("M d, Y", strtotime($con_date));
            							?>
            						</span>
            					</div>
            					<p class="lead">
            						{!!$row->description!!}
            					</p>
            				</div>
            			</div>

            			@endif

            		</div>
            		@endforeach

            	</div>
            </section>
            @else

            @endif












            <!-- <span>{{$barangay_officials[0]->position}}</span> -->

            
            @if(count($barangay_officials) != 0)
            <section class="text-center">
            	<div class="container">

            		<div class="row">

            			@foreach($barangay_officials as $row)

            			@if($row->position == "SK Member")
            			<div class="col-md-4">
            				<div class="feature feature-8">
            					<img alt="Image" style="object-fit: cover; height: 250px; width: 80%;" src="{{ asset('storage/barangay_officials_images')}}/{{$row->display_image}}" />
            					<h5>{{$row->barangay_official_name}}</h5>
            					<span>{{$row->position}}</span>
            					<br>
            					<span>Date Elected:</span>
            					<span>
            						<?php
            						$orig_date =  explode('-', $row->date_elected);
            						$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
            						echo date("M d, Y", strtotime($con_date));
            						?>
            					</span>
            				</div>
            			</div>


            			@endif
            			@endforeach
            		</div>
            		

            	</div>

            </section>

            @else

            @endif




            

            
          </div>







          @endsection