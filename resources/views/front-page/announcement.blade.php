@extends('welcome')

@section('title')

Bislig | Announcement
@endsection

@section('content')


<div class="main-container">
	<section class="unpad">
		<div class="slider" data-arrows="true">
			<ul class="slides slides--gapless">


				@foreach($barangay_announcement_limit as $row)

				<li class="col-md-6 col-12">
					<a class="block" href="{{ url('/announcement-details', $row->barangay_name)}}/{{\Crypt::encrypt($row->announcement_id)}}">
						<article class="imagebg" data-scrim-bottom="8">
							<div class="background-image-holder">

								@if($row->display_image != 'noimage.jpg')

								<img alt="background" src="{{ asset('storage/announcement_images')}}/{{$row->display_image}}" />

								@else

								<img alt="background" src="{{ asset('noimage.jpg')}}" />

								@endif

							</div>
							<div class="article__title">

								<span>
									<?php
									$orig_date =  explode('-', $row->date_created);
									$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
									echo date("M d, Y", strtotime($con_date));
									?>                
								</span>

								<h3 style="margin-bottom: 2px; overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"><b>{{$row->title}}</b></h3>

								<span class="lead" style="overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">
									{!!$row->description!!}
									<!-- @php 
									echo strip_tags($row->description);
									@endphp -->

								</span>
								<br>
							</div>
						</article>
					</a>
				</li>

				@endforeach


			</ul>
		</div>
	</section>








	@if(count($barangay_announcement_paginate) != 0)
	<section>
		<h1 class="text-center">Announcements</h1>
		<div class="container">

			<form method="POST" action="{{ url('/announcement/search', $barangay_announcement_paginate[0]->barangay_name)}}" class="form--horizontal row m-0">
				{{ csrf_field() }}

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="col-md-8">
					<input type="text" name="q" placeholder="Type search keywords here" required="true"/>
				</div>
				<div class="col-md-4">
					<button type="submit" class="btn btn--primary type--uppercase">Search</button>
				</div>
			</form>

			@if(isset($details))
			<p>The Search results for your query <br>
				<b> {{$query}} </b> are:</p>
				@endif

				<hr>

				<div class="row">
					<div class="col-md-12">
						<div class="masonry">
							<div class="masonry__container row">

								@foreach($barangay_announcement_paginate as $key => $announcement_paginate)

								<div class="masonry__item col-lg-12">
									<article class="feature feature-1 boxed boxed--border">
										<div class="row">
											<div class="col-md-4" style="padding: 26px 0px 24px 0px">


												@if($announcement_paginate->display_image != 'noimage.jpg')

												<a href="{{ url('/announcement-details', $announcement_paginate->barangay_name)}}/{{\Crypt::encrypt($announcement_paginate->announcement_id)}}" class="block">
													<img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/announcement_images')}}/{{$announcement_paginate->display_image}}" />
												</a>


												@else

												<a href="{{ url('/announcement-details', $announcement_paginate->barangay_name)}}/{{\Crypt::encrypt($announcement_paginate->announcement_id)}}" class="block">
													<img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('noimage.jpg')}}" />
												</a>

												@endif



											</div>
											<div class="col-md-8">
												<div class="feature__body boxed">

													<span>
														<?php
														$orig_date =  explode('-', $announcement_paginate->date_created);
														$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
														echo date("M d, Y", strtotime($con_date));
														?>  
													</span>

													<h3 style="margin-bottom: 2px; overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"><b>{{$announcement_paginate->title}}</b></h3>

													<span class="lead" style="overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">

														{!!$announcement_paginate->description!!}

													<!-- @php 
													echo strip_tags($announcement_paginate->description);
													@endphp -->

												</span>

												<a href="{{ url('/announcement-details', $announcement_paginate->barangay_name)}}/{{\Crypt::encrypt($announcement_paginate->announcement_id)}}">
													Read More
												</a>
											</div>
										</div>
									</div>
								</article>
							</div>

							@endforeach

						</div>


						<div class="pagination">
							<ol>

								{!!$barangay_announcement_paginate->render() !!}

							</ol>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	@else

	<section>
		<div class="col-md-12" align="center">
			<h2 style="text-align: center; color: grey"> ---- no announcement yet ----</h2>
		</div>
	</section>

	@endif



</div>


@endsection
