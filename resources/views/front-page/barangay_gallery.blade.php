@extends('welcome')

@section('title')

Bislig | Gallery
@endsection

@section('content')
<div class="main-container">
	<section class="cover height-30 imagebg text-center slider" data-arrows="true" data-paging="true">
		<ul class="slides">
			<li class="imagebg" data-overlay="4">
				<div class="background-image-holder background--top">
					<img alt="background" src="{{ asset('frontendfiles/img/tinuy-an.jpg')}}" />
				</div>
				<div class="container pos-vertical-center">
					<div class="row">
						<div class="col-md-12">
							<h1>
								Barangay {{$barangay[0]->barangay_name}} Photos
							</h1>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</section>

	<section class=" ">
		<div class="container">
			<div class="masonry">

				<hr />

				@if(count($barangaygallery) !=0 )
				<div class="masonry__container row">


					@foreach($barangaygallery as $key => $row)

					<div class="masonry__item col-md-6 col-12" data-masonry-filter="Print" >
						<div class="project-thumb hover-element border--round hover--active">
							<a href="{{ asset('storage/barangay_gallery_images')}}/{{$row->display_image}}" data-lightbox="gallery">
								<div class="hover-element__initial">
									<div class="background-image-holder">
										<img alt="background" src="{{ asset('storage/barangay_gallery_images')}}/{{$row->display_image}}" />
									</div>
								</div>
								<div class="hover-element__reveal" data-scrim-top="5">
									<div class="project-thumb__title">

										<span>{{$row->date_taken}}



										</span>
									</div>
								</div>
							</a>
						</div>
					</div>

					@endforeach


				</div>


				@else

				<div class="col-md-12" align="center">
					<h3 style="color: red">--No image, Please upload image in-- <a href="{{url('/dashboard')}}" style="color: black; font-weight: inherit;">"Dashboard"</a></h3>
				</div>

				@endif
			</div>
			
		</div>
	</section>

</div>




























@endsection