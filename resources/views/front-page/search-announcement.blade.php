<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<title>Search | Announcement


	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Site Description Here">

	<link href="{{ asset('frontendfiles/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/stack-interface.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/flickity.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/iconsmind.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/jquery.steps.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/theme-greensea.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontendfiles/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="icon" href="{{ asset('frontendfiles/img/bislig-seal.png')}}" type="image/gif" sizes="16x16">
</head>
<body class=" ">
	<a id="start"></a>
	<div class="nav-container" style="min-height: 100px !important;">
		<div class="via-1611198644230" via="via-1611198644230" vio="bislig">
			<div class="bar bar--sm visible-xs">
				<div class="container">
					<div class="row">
						<div class="col-3 col-md-2">
							<a href="index.html"> <img class="logo logo-dark" alt="logo" src="img/bislig-header-dark.png"> <img class="logo logo-light" alt="logo" src="{{ asset('frontendfiles/img/bislig-header-dark.png')}}"> </a>
						</div>
						<div class="col-9 col-md-10 text-right">
							<a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="icon icon--sm stack-interface stack-menu"></i> </a>
						</div>
					</div>
				</div>
			</div>
			<nav id="menu1" class="bar bar-1 hidden-xs" data-scroll-class="50vh:pos-fixed">
				<div class="container">
					<div class="row">
						<div class="col-lg-1 col-md-2 hidden-xs">
							<div class="bar__module">
								<a href="index.html"> <img class="logo logo-dark" alt="logo" src="{{ asset('frontendfiles/img/bislig-header-dark.png')}}"> <img class="logo logo-light" alt="logo" src="{{ asset('frontendfiles/img/bislig-header-dark.png')}}"> </a>
							</div>
						</div>
						<div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
							<div class="bar__module" style="margin-top: 15px;">
								<ul class="menu-horizontal text-left">



									<li> <a href="{{url('/', $barangay[0]->barangay_name)}}">Home</a> </li>
									<li> <a href="{{url('/about-barangay', $barangay[0]->barangay_name)}}">About</a> </li>

									<li> <a href="{{url('barangay-officials', $barangay[0]->barangay_name)}}">Barangay Officials</a> </li>



									<li class="dropdown"> <span class="dropdown__trigger">Gallery&nbsp;</span>
										<div class="dropdown__container">
											<div class="container">
												<div class="row">
													<div class="dropdown__content col-lg-2">
														<ul class="menu-vertical">


															<li> <a href="{{ url('barangay-gallery', $barangay[0]->barangay_name)}}">Photos</a> </li>
															<li> <a href="{{ url('barangay-videos', $barangay[0]->barangay_name)}}">Videos</a> </li>


														</ul>
													</div>
												</div>
											</div>
										</div>
									</li>




									<li> <a href="{{url('/barangay/announcement', $barangay[0]->barangay_name)}}">Announcements</a> </li>
								</ul>
							</div>




							<div class="bar__module">
								<a class="btn btn--sm type--uppercase" href="#customise-template"> <span class="btn__text">
									Sample
								</span> </a>
								<a class="btn btn--sm btn--primary type--uppercase" href="#purchase-template"> <span class="btn__text">
									Sample
								</span> </a>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>










	<div class="main-container">




		<section>
			<div class="container">
				<form method="POST" action="{{ url('/announcement/search', $barangay[0]->barangay_name)}}" class="form--horizontal row m-0">
					{{ csrf_field() }}

					<div class="col-md-8">
						<input type="text" name="q" placeholder="Type search keywords here" required="true"/>
					</div>
					<div class="col-md-4">
						<button type="submit" class="btn btn--primary type--uppercase">Search</button>
					</div>
				</form>

				<hr>
				@if(isset($details))
				<br>

				<div class="row">
					<h3>Search Result for <span style="font-weight: 50px;">"{{$query}}"</span> </h3>
				</div>


				<div class="row">
					<div class="col-md-12">
						<div class="masonry">
							<div class="masonry__container row">


								@foreach($details as $key => $result)

								<div class="masonry__item col-lg-12">
									<article class="feature feature-1 boxed boxed--border">
										<div class="row">
											<div class="col-md-4" style="padding: 26px 0px 24px 0px">

												@if($result->display_image != 'noimage.jpg')

												<a href="{{ url('/announcement-details', $result->barangay_name)}}/{{\Crypt::encrypt($result->announcement_id)}}" class="block">
													<img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('storage/announcement_images')}}/{{$result->display_image}}" />
												</a>

												@else

												<a href="{{ url('/announcement-details', $result->barangay_name)}}/{{\Crypt::encrypt($result->announcement_id)}}" class="block">
													<img alt="Image" style="object-fit: cover; width: 100%; height: 240px;" src="{{ asset('noimage.jpg')}}" />
												</a>

												@endif

											</div>
											<div class="col-md-8">
												<div class="feature__body boxed">

													<span>
														<?php
														$orig_date =  explode('-', $result->date_created);
														$con_date = $orig_date[0].'-'.$orig_date[1].'-'.$orig_date[2];
														echo date("M d, Y", strtotime($con_date));
														?>  
													</span>


													<h3 style="margin-bottom: 2px; overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;"><b>{{$result->title}}</b></h3>


													<span class="lead" style="overflow: hidden;text-overflow: ellipsis; display: block; display: -webkit-box; max-width: 100%; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;">

														{!!$result->description!!}

													<!-- @php 
													echo strip_tags($result->description);
													@endphp -->

												</span>

												<a href="{{ url('/announcement-details', $result->barangay_name)}}/{{\Crypt::encrypt($result->announcement_id)}}">
													Read More
												</a>
											</div>
										</div>
									</div>
								</article>
							</div>

							@endforeach


						</div>


						@if(count($details) != 0)

						<div class="pagination">
							<ol>

								{!!$details->render() !!}

							</ol>
						</div>

						@endif

					</div>
					@elseif(isset($message))
					<p>{{$message}}</p>
					@endif
				</div>
			</div>
		</div>
		
	</section>





</div>




















<footer class="footer-6 unpad--bottom  bg--dark ">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-3">
				<h6 class="type--uppercase">About Us</h6>
				<p>
					Stack is a robust multipurpose HTML template designed with modularity at the core. Easily combine over 290 interface blocks to create websites for any purpose.
				</p>
			</div>
			<div class="col-md-6 col-lg-3">
				<h6 class="type--uppercase">Recent Updates</h6>
				<div class="tweets-feed tweets-feed-2" data-feed-name="mrareweb" data-amount="2"></div>
			</div>
			<div class="col-md-6 col-lg-3">
				<h6 class="type--uppercase">Instagram</h6>
				<div class="instafeed instafeed--gapless" data-user-name="mediumrarethemes" data-amount="6" data-grid="3"></div>
			</div>
			<div class="col-md-6 col-lg-3">
				<h6 class="type--uppercase">Newsletter</h6>
				<form action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your email address.">
					<input class="validate-required validate-email" type="text" name="EMAIL" placeholder="Email Address" />
					<button type="submit" class="btn btn--primary type--uppercase">Subscribe</button>
					<div style="position: absolute; left: -5000px;" aria-hidden="true">
						<input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
					</div>
				</form>
			</div>
		</div>
		<!--end of row-->
	</div>
	<!--end of container-->
	<div class="footer__lower text-center-xs">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<span class="type--fine-print">&copy;
						<span class="update-year"></span> Bislig &mdash; All Rights Reserved</span>
					</div>
					<div class="col-md-6 text-right text-center-xs">
						<ul class="social-list list-inline">
							<li>
								<a href="#">
									<i class="socicon socicon-google icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="socicon socicon-twitter icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="socicon socicon-facebook icon icon--xs"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="socicon socicon-instagram icon icon--xs"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!--end of row-->
			</div>
			<!--end of container-->
		</div>
	</footer>

        <!--<div clas
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        	<i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ asset('frontendfiles/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/flickity.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/easypiechart.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/parallax.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/typed.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/datepicker.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/isotope.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/ytplayer.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/lightbox.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/granim.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/jquery.steps.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/countdown.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/twitterfetcher.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/spectragram.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/smooth-scroll.min.js')}}"></script>
        <script src="{{ asset('frontendfiles/js/scripts.js')}}"></script>
    </body>
    </html>